package com.fishbowl.importer.converter;

import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.configuration.MappedColumn;
import com.fishbowl.importer.utils.Util;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 5/13/2016.
 */
public class RowComparator implements Comparable<RowComparator> {
    private int hash_code = super.hashCode();
    private String duplicate;
    private String same;
    private List<String> first_row;
    private List<List<String>> child_rows;
    private boolean duplicated = false;
    private List<String> uniqueIds = new ArrayList<>();
    private Integer uniqueColumn = null;


    public void refresh() {
        if (uniqueColumn == null)
            return;
        if (uniqueIds.isEmpty())
            return;
        String value = "";
        for (String uniqueId : uniqueIds) {
            if (!value.isEmpty())
                value += " & ";
            value += uniqueId;
        }
        first_row.set(uniqueColumn, value);
    }

    public void addUnique(String unique) {
        if (unique == null)
            return;
        if (uniqueIds.contains(unique))
            return;
        uniqueIds.add(unique);
    }

    public RowComparator(Customer customer, String[] first_row, String[] second_row) {
        HashCodeBuilder builder = new HashCodeBuilder();
        duplicate = "";
        same = "";
        if (customer.getDuplicateColumn() != null) {
            uniqueColumn = getUniqueColumn(customer.getDuplicateColumn(), getRow(customer.getDuplicateColumn(), first_row, second_row));
            String value = getValuesFrom(customer.getDuplicateColumn(), first_row, second_row);
            if (value != null)
                duplicate = value;
        }
        if (Util.notEmpty(customer.getSameColumns())) {
            for (MappedColumn column : customer.getSameColumns()) {
                String value = getValuesFrom(column, first_row, second_row);
                if (value != null) {
                    if (Util.notEmpty(same))
                        same += "_";
                    same += value;
                }

            }

        }
        if (Util.notEmpty(duplicate)) {
            builder.append(duplicate);
            hash_code = builder.hashCode();
        }

//        if (Util.notEmpty(duplicate) || Util.notEmpty(same)) {
//            hash_code = 0;
//
////            if (Util.notEmpty(same))
////                builder.append(same);
////            hash_code=
//        }

    }

    private static String[] getRow(MappedColumn mappedColumn, String[] first_row, String[] second_row) {
        if (mappedColumn == null)
            return null;
        String[] row = first_row;
        if (Util.has(mappedColumn.getSecondRow()) && second_row != null) {
            row = second_row;
        }
        return row;
    }

    public static String getValuesFrom(MappedColumn mappedColumn, String[] first_row, String[] second_row) {
        String[] row = getRow(mappedColumn, first_row, second_row);
        if (row == null)
            return null;
        Integer column = getUniqueColumn(mappedColumn, row);
        if (column == null)
            return null;
        return row[column];
    }

    public static Integer getUniqueColumn(MappedColumn mappedColumn, String[] row) {
        if (mappedColumn == null)
            return null;
        if (row == null)
            return null;
        Integer column = mappedColumn.getSource();
        if (column == null)
            column = mappedColumn.getDest();
        if (column == null || column < 0)
            return null;
        if (column + 1 > row.length)
            return null;
        return column;
    }

    @Override
    public boolean equals(Object obj) {
        if (Util.isEmpty(duplicate) && Util.isEmpty(same))
            return false;
        if (super.equals(obj))
            return true;
        if (obj instanceof RowComparator) {
            RowComparator objR = (RowComparator) obj;

            String duplicate = objR.duplicate;
            String same = objR.same;

            boolean result = this.duplicate.equals(duplicate) || this.same.equals(same);
            return result;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return hash_code;
    }

    public String getDuplicate() {
        return duplicate;
    }

    @Override
    public int compareTo(RowComparator o) {
        if (equals(o))
            return 0;
        try {
            Integer o1I = Integer.parseInt(this.getDuplicate());
            Integer o2I = Integer.parseInt(o.getDuplicate());
            int comp = o1I.compareTo(o2I);
            return comp;
        } catch (Exception e) {
            int comp = this.getDuplicate().compareTo(o.getDuplicate());
            return comp;
        }

    }

    public List<String> getFirst_row() {
        return first_row;
    }

    public void setFirst_row(List<String> first_row) {
        this.first_row = first_row;
    }

    public List<List<String>> getChild_rows() {
        return child_rows;
    }

    public void setChild_rows(List<List<String>> child_rows) {
        this.child_rows = child_rows;
    }


    public void addChild_row(List<String> child_row) {
        if (child_rows == null)
            child_rows = new ArrayList<>();
        child_rows.add(child_row);
    }

    public void checkDuplicated(List<String> uniques) {
        duplicated=false;
        if (Util.notEmpty(uniques) && Util.notEmpty(uniqueIds)){
            for (String uniqueId : uniqueIds) {
                if(uniques.contains(uniqueId))
                {
                    duplicated=true;
                    break;
                }
            }
        }
//            duplicated = uniques.contains(getDuplicate());
    }

    public boolean isDuplicated() {
        return duplicated;
    }
}
