package com.fishbowl.importer.converter;

/**
 * Created by dimitri.gamkrelidze on 4/15/2016.
 */
public interface CSVEventListener {
    public void proceed(int size, int currentPos);
}
