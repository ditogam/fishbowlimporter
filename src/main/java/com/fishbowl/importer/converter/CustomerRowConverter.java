package com.fishbowl.importer.converter;

import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.configuration.MappedColumn;
import com.fishbowl.importer.utils.Util;
import org.apache.commons.beanutils.PropertyUtils;

import java.util.*;

/**
 * Created by dimitri.gamkrelidze on 4/13/2016.
 */
public class CustomerRowConverter {
    private Customer customer;
    private int max_columns;
    private Map<String, Object> customerColumns;
    private List<MappedColumnConverter> columnConverters;
    private List<RowComparator> rowComparators = new ArrayList<>();


    private List<String> ids = new ArrayList<>();

    public CustomerRowConverter(Customer customer) throws Exception {
        this.customer = customer;
        customer.parse();
        max_columns = customer.getMaxColumns();
        customerColumns = PropertyUtils.describe(customer);
        columnConverters = new ArrayList<>();
        for (MappedColumn column : customer.getMappedColumns()) {
            columnConverters.add(new MappedColumnConverter(column, this));
        }

    }

    public void proceedValues(List<String> record, Map<Integer, Map<Integer, Map<String, String>>> mapping) {


        String[] first_row = new String[max_columns];
        String[] second_row = customer.isTwoRows() ? new String[max_columns] : null;

        for (MappedColumnConverter converter : columnConverters) {
            try {
                converter.convert(record, first_row, second_row, mapping);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        RowComparator rowComparator = new RowComparator(customer, first_row, second_row);
        String id = rowComparator.getDuplicate();
        if (id != null && !ids.contains(id))
            ids.add(id);


        RowComparator result = null;
        try {
            int index = rowComparators.indexOf(rowComparator);
            if (index > -1)
                result = rowComparators.get(index);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result != null && second_row != null) {
            result.addChild_row(Arrays.asList(second_row));

        } else {
            result = rowComparator;
            rowComparator.setFirst_row(Arrays.asList(first_row));
            if (second_row != null)
                result.addChild_row(Arrays.asList(second_row));
            rowComparators.add(rowComparator);
        }
        result.addUnique(id);
    }

    public Object getCustomerProperty(String property) {
        return customerColumns.get(property);
    }

    public Customer getCustomer() {
        return customer;
    }

    public List<String> getIds() {
        return ids;
    }

    public Collection<List<List<String>>> getRecords() {
        Collection<List<List<String>>> result = new ArrayList<>();
        for (RowComparator rowComparator : rowComparators) {
            List<List<String>> list = rowComparator.getChild_rows();
            if (Util.isEmpty(list))
                continue;
            result.add(Arrays.asList(rowComparator.getFirst_row()));
            result.add(list);
        }
        return result;
    }

    public int getMax_columns() {
        return max_columns;
    }

    public void checkDuplicated(List<String> uniques) {
        if (Util.notEmpty(uniques))
            for (RowComparator rowComparator : rowComparators) {
                rowComparator.checkDuplicated(uniques);
            }
    }

    public List<RowComparator> getRowComparators() {
        return rowComparators;
    }

    public void refresh() {
        rowComparators.forEach(RowComparator::refresh);
    }
}
