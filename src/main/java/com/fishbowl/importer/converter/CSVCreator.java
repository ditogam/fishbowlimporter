package com.fishbowl.importer.converter;

import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.utils.Util;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dimitri.gamkrelidze on 4/13/2016.
 */
public class CSVCreator implements CSVEventListener {
    private static List<List<String>> headers = new ArrayList<>();

    static {

        try (Reader myreader = new InputStreamReader(ClassLoader.getSystemResourceAsStream("header.csv")); CSVParser parser = new CSVParser(myreader, CSVFormat.DEFAULT)) {
            for (CSVRecord record : parser.getRecords()) {
                int size = record.size();
                List<String> rec = new ArrayList<>(size);
                for (int i = 0; i < size; i++) {
                    rec.add(record.get(i));
                }
                headers.add(rec);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<CSVEventListener> eventListeners = new ArrayList<>();

    public CSVCreator addEventListeners(CSVEventListener listener) {
        eventListeners.add(listener);
        return this;
    }

    public Map<Integer, Map<Integer, Map<String, String>>> parse(Customer customer, Reader reader, InputStream stream, CustomerRowConverter converter, boolean pdf, Integer maxRecords) throws Exception {
        Map<Integer, Map<Integer, Map<String, String>>> pdf_mappings = null;
        try (Reader myreader = reader; InputStream myPdfreader = stream; CSVParser parser = new CSVParser(myreader, CSVFormat.DEFAULT)) {

            if (customer.isPdfColumns() && stream != null) {
                pdf_mappings = PdfParser.createPdfMappings(customer.getPdfColumnsIndexes(), stream, pdf);
            }


//            addRecords(csvPrinter, headers);
            List<CSVRecord> records = null;
            long time = System.currentTimeMillis();
            if (maxRecords != null) {
                records = Util.getCsvRecordsRowNum(maxRecords, parser);
            } else
                records = parser.getRecords();

            int current = 1;
            boolean header_proceeded = Util.has(customer.getFistRowHeader());
            int fullSize = records.size();
            Map<Integer, Long> maxTimes = new HashMap<>();
            System.err.println("Reading " + fullSize + " records in " + (System.currentTimeMillis() - time));
            if (header_proceeded)
                fullSize--;
            long fulltime = 0;
            int cnt = 0;
            long min = Long.MAX_VALUE;
            long max = Long.MIN_VALUE;
            for (CSVRecord record : records) {
                if (!header_proceeded) {
                    header_proceeded = true;
                    continue;
                }
                time = System.currentTimeMillis();
                int size = record.size();
                List<String> rec = new ArrayList<>(size);
                for (int i = 0; i < size; i++) {
                    rec.add(record.get(i));
                }
                converter.proceedValues(rec, pdf_mappings);
                time = System.currentTimeMillis() - time;
                fulltime += time;
                cnt++;
                if (time > max)
                    maxTimes.put(cnt, time);
                min = Long.min(min, time);
                max = Long.max(max, time);

                if (cnt % 10000 == 0)
                    System.err.println("Min = " + min + " Max = " + max + " Current = " + time + " Cnt = " + cnt + " Avg = " + (fulltime / cnt));

                proceed(fullSize, current++);
            }
            System.err.println("Full Min = " + min + " Max = " + max + " Current = " + time + " Cnt = " + cnt + " Avg = " + (fulltime / cnt) + " full time=" + fulltime + " MaxTimes=" + maxTimes);
//            for (List<List<String>> lists : converter.getRecords()) {
//                addRecords(csvPrinter, lists);
//            }
        }
        return pdf_mappings;
    }

    private int addRecords(CSVPrinter csvPrinter, List<List<String>> result, int full_size, int cur_pos) throws IOException {
        for (List<String> strings : result) {
            csvPrinter.printRecord(strings);
            proceed(full_size, ++cur_pos);
        }
        return cur_pos;
    }

    public void save(String file, List<List<String>> result) throws Exception {
        try (Writer writer = new FileWriter(new File(file)); CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);) {
            int curPos = 0;
            int full_size = headers.size() + result.size();

            curPos = addRecords(csvPrinter, headers, full_size, curPos);
            curPos = addRecords(csvPrinter, result, full_size, curPos);
        }
    }

    @Override
    public void proceed(int size, int currentPos) {
        for (CSVEventListener eventListener : eventListeners) {
            eventListener.proceed(size, currentPos);
        }
    }
}
