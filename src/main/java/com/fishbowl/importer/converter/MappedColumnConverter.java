package com.fishbowl.importer.converter;

import com.fishbowl.importer.configuration.MappedColumn;
import com.fishbowl.importer.utils.Util;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by dimitri.gamkrelidze on 4/13/2016.
 */
public class MappedColumnConverter {
    private MappedColumn column;
    private CustomerRowConverter converter;
    private List<MappedColumnConverter> columnConverters;

    public MappedColumnConverter(MappedColumn column, CustomerRowConverter converter) {
        this.column = column;

        this.converter = converter;
        if (Util.notEmpty(column.getConcatenatedColumns())) {
            columnConverters = new ArrayList<>();
            for (MappedColumn mappedColumn : column.getConcatenatedColumns()) {
                columnConverters.add(new MappedColumnConverter(mappedColumn, converter));
            }
        }
    }

    public void convert(List<String> record, String[] first_row, String[] second_row, Map<Integer, Map<Integer, Map<String, String>>> mapping) throws Exception {
        if (column.getDest() == null)
            return;
        int index = column.getDest();

        String[] row = Util.has(column.isSecondRow()) ? second_row : first_row;
        Object obj = getValue(record);
        if (Util.isEmpty(columnConverters) && obj != null)
            setValue(row, index, obj, mapping);
        else {
            if (Util.notEmpty(columnConverters)) {
                boolean assigned = false;
                StringBuilder sb = new StringBuilder();
                for (MappedColumnConverter columnConverter : columnConverters) {
                    String value = columnConverter.convert(record, mapping);
                    if (Util.notEmpty(value)) {
                        if (assigned)
                            sb.append(column.getConcatenator() == null ? "" : column.getConcatenator());
                        assigned = true;
                        sb.append(value.trim());
                    }
                }
                setValue(row, index, sb.toString().trim(), mapping);
            }
        }

    }

    private Object getValue(List<String> record) {
        if (Util.notEmpty(column.getStaticValue())) {
            return column.getStaticValue();
        }
        if (Util.notEmpty(column.getFieldFromCustomer())) {
            return converter.getCustomerProperty(column.getFieldFromCustomer());
        }
        if (column.getSource() != null) {
            int sourceIndex = column.getSource();
            if (sourceIndex > record.size() - 1 || sourceIndex < 0)
                return null;
            return record.get(sourceIndex);
        }
        return null;
    }

    private String convert(List<String> record, Map<Integer, Map<Integer, Map<String, String>>> mapping) throws Exception {
        return convert(getValue(record), mapping);
    }


    private Object getMappedValue(Object value, Map<Integer, Map<Integer, Map<String, String>>> mapping) {
        if (mapping == null || mapping.isEmpty())
            return value;
        if (value == null)
            return null;
        if (Util.isNullOrNegative(column.getSourcePDF()) || Util.isNullOrNegative(column.getDestPDF()))
            return value;

        Map<Integer, Map<String, String>> mapSource = mapping.get(column.getSourcePDF());
        if (mapSource == null)
            return null;
        Map<String, String> map = mapSource.get(column.getDestPDF());
        if (map == null)
            return null;
        value = map.get(value.toString());
        return value;
    }

    private String convert(Object value, Map<Integer, Map<Integer, Map<String, String>>> mapping) throws Exception {
        if (value == null)
            return null;
        value = getMappedValue(value, mapping);
        if (value == null)
            return null;
        if (Util.has(column.isDate())) {
            DateFormat dfSource = Util.getDateFormat(column.getSourceDateFormat(), converter.getCustomer().getDefaultSourceDateFormat());
            DateFormat dfDest = Util.getDateFormat(column.getDestDateFormat(), converter.getCustomer().getDefaultDestDateFormat());
            if (dfSource != null)
                value = dfSource.parse(value.toString());
            if (value instanceof Date && dfDest != null)
                value = dfDest.format((Date) value);
            else if (dfDest != null)
                value = dfDest.format(dfDest.parse(value.toString()));
        }
        if (Util.has(column.isNumber())) {
            NumberFormat dfSource = Util.getNumberFormat(column.getSourceNumberFormat(), converter.getCustomer().getDefaultSourceNumberFormat());
            NumberFormat dfDest = Util.getNumberFormat(column.getDestNumberFormat(), converter.getCustomer().getDefaultDestNumberFormat());
            if (dfSource != null)
                value = dfSource.parse(value.toString());
            if (value instanceof Number && dfDest != null)
                value = dfDest.format((Number) value);
            else if (dfDest != null)
                value = dfDest.format(dfDest.parse(value.toString()));
        }
        return value.toString().trim();
    }

    private void setValue(String[] row, int index, Object value, Map<Integer, Map<Integer, Map<String, String>>> mapping) throws Exception {
        if (value != null)
            row[index] = convert(value.toString(), mapping);
    }
}
