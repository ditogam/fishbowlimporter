package com.fishbowl.importer.converter;

import com.fishbowl.importer.configuration.Pair;
import com.fishbowl.importer.utils.Util;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dito on 4/16/16.
 */
public class PdfParser {


    public static void writePDF(InputStream is, OutputStream os) throws IOException {
        PdfReader reader = new PdfReader(is);
        try (InputStream stream = is) {
            for (int i = 0; i < reader.getNumberOfPages(); i++) {
                String
                        value = PdfTextExtractor.getTextFromPage(reader, i + 1);
                os.write(value.getBytes(StandardCharsets.UTF_8));
            }
        }
    }


    public static Map<Integer, Map<Integer, Map<String, String>>> createPdfMappings(List<Pair<Integer, Integer>> columns, InputStream stream, boolean pdf) throws Exception {
        Map<Integer, Map<Integer, Map<String, String>>> result = new HashMap<>();
        CSVFormat format = CSVFormat.DEFAULT;
        if (pdf) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            writePDF(stream, bos);
            byte[] bytes = bos.toByteArray();
            bos.close();
            stream = new ByteArrayInputStream(bytes);
            format = format.withDelimiter(' ');
        }
        try (CSVParser parser = new CSVParser(new InputStreamReader(stream), format)) {
            for (CSVRecord strings : parser.getRecords()) {
                int size = strings.size();
                if (size == 0)
                    continue;
                for (Pair<Integer, Integer> column : columns) {
                    if (Util.between(column.getLeft() + 1, 1, size) && Util.between(column.getRight() + 1, 1, size)) {
                        String source = strings.get(column.getLeft());
                        String dest = strings.get(column.getRight());
                        if (Util.isEmpty(source) || Util.isEmpty(dest))
                            continue;
                        Map<Integer, Map<String, String>> mapSource = result.get(column.getLeft());
                        if (mapSource == null) {
                            mapSource = new HashMap<>();
                            result.put(column.getLeft(), mapSource);
                        }
                        Map<String, String> mapDest = mapSource.get(column.getRight());
                        if (mapDest == null) {
                            mapDest = new HashMap<>();
                            mapSource.put(column.getRight(), mapDest);
                        }
                        mapDest.put(source.trim(), dest.trim());
                    }
                }
            }
        }
        return result;
    }
}
