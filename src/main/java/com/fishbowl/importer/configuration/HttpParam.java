package com.fishbowl.importer.configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fishbowl.importer.utils.Util;

/**
 * Created by dimitri.gamkrelidze on 4/19/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HttpParam {
    @JsonProperty("param_name")
    private String paramName;

    @JsonProperty("param_value")
    private String paramValue;

    @JsonProperty("request_body")
    private Boolean requestBody;

    @JsonProperty("request_part")
    private Boolean requestPart;

    public HttpParam() {

    }

    public HttpParam(String paramName, String paramValue, Boolean requestBody, Boolean requestPart) {
        this.paramName = paramName;
        this.paramValue = paramValue;
        setRequestBody(requestBody);
        setRequestPart(requestPart);
    }

    public HttpParam(String paramName, String paramValue) {
        this(paramName, paramValue, null, null);
    }

    public String getParamName() {
        return paramName;
    }

    public HttpParam setParamName(String paramName) {
        this.paramName = paramName;
        return this;
    }

    public String getParamValue() {
        return paramValue;
    }

    public HttpParam setParamValue(String paramValue) {
        this.paramValue = paramValue;
        return this;
    }

    public Boolean getRequestBody() {
        return requestBody;
    }

    public HttpParam setRequestBody(Boolean requestBody) {
        this.requestBody = Util.has(requestBody) ? true : null;
        return this;
    }

    public Boolean getRequestPart() {
        return requestPart;
    }

    public HttpParam setRequestPart(Boolean requestPart) {
        this.requestPart = Util.has(requestPart) ? true : null;
        return this;
    }
}
