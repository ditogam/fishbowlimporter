package com.fishbowl.importer.configuration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fishbowl.importer.utils.Util;
import org.apache.http.entity.ContentType;
import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dimitri.gamkrelidze on 4/18/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HttpParams {
    @JsonProperty("url")
    private String url;

    @JsonProperty("method")
    private String method;

    @JsonProperty("content_type")
    private String contentType;

    @JsonProperty("headers")
    private Map<String, String> headers;

    @JsonProperty("params")
    private List<HttpParam> params;


    public String getUrl() {
        return url;
    }

    public HttpParams setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getMethod() {
        return method;
    }

    public HttpParams setMethod(String method) {
        if (Util.notEmpty(method) && method.toUpperCase().equals(HttpMethod.GET.toString().toUpperCase()))
            method = null;
        this.method = method;
        return this;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public HttpParams setHeaders(Map<String, String> headers) {
        if (Util.isEmpty(headers))
            headers = null;
        this.headers = headers;
        return this;
    }

    public List<HttpParam> getParams() {
        return params;
    }

    public HttpParams setParams(List<HttpParam> params) {
        if (Util.isEmpty(params))
            params = null;
        this.params = params;
        return this;
    }

    public String getContentType() {
        return contentType;
    }

    public HttpParams setContentType(String contentType) {
        if (Util.notEmpty(contentType) && contentType.toUpperCase().equals(ContentType.APPLICATION_JSON.toString().toUpperCase()))
            contentType = null;
        this.contentType = contentType;
        return this;
    }

    @JsonIgnore
    public HttpParams addParams(String key, String value) {
        if (params == null)
            params = new ArrayList<>();
        params.add(new HttpParam(key, value));
        return this;
    }

    @JsonIgnore
    public HttpParams addParams(String key, String value, Boolean requestBody, Boolean requestPart) {
        if (params == null)
            params = new ArrayList<>();
        params.add(new HttpParam(key, value, requestBody, requestPart));
        return this;
    }
}
