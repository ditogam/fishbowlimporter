package com.fishbowl.importer.configuration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fishbowl.importer.utils.ColumnConverter;
import com.fishbowl.importer.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 4/13/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MappedColumn {
    @JsonProperty("source")
    private Integer source;

    @JsonProperty("dest")
    private Integer dest;

    @JsonProperty("source_a")
    private String sourceA;

    @JsonProperty("dest_a")
    private String destA;

    @JsonProperty("second_row")
    private Boolean secondRow;

    @JsonProperty("date")
    private Boolean date;

    @JsonProperty("source_date_format")
    private String sourceDateFormat;

    @JsonProperty("dest_date_format")
    private String destDateFormat;

    @JsonProperty("number")
    private Boolean number;

    @JsonProperty("source_number_format")
    private String sourceNumberFormat;

    @JsonProperty("dest_number_format")
    private String destNumberFormat;

    @JsonProperty("field_from_customer")
    private String fieldFromCustomer;

    @JsonProperty("static_value")
    private String staticValue;
    /**
     * needed for calculation fields
     */
    @JsonProperty("concatenator")
    private String concatenator;


    @JsonProperty("source_pdf")
    private Integer sourcePDF;

    @JsonProperty("dest_pdf")
    private Integer destPDF;

    @JsonProperty("source_a_pdf")
    private String sourceAPDF;

    @JsonProperty("dest_a_pdf")
    private String destAPDF;


    /**
     * needed for calculation fields
     */
    @JsonProperty("concatenated_columns")
    private List<MappedColumn> concatenatedColumns;

    public static MappedColumn setCouple(Integer source, Integer dest, Boolean secondRow) {
        MappedColumn value = new MappedColumn();
        value.setSource(source);
        value.setDest(dest);
        value.setSecondRow(secondRow);
        return value;
    }

    public static MappedColumn setCouple(Integer source, Integer dest) {
        MappedColumn value = new MappedColumn();
        value.setSource(source);
        value.setDest(dest);
        return value;
    }

    public static MappedColumn setCouple(String source, String dest, Boolean secondRow) {
        return setCouple(source, dest).setSecondRow(secondRow);
    }

    public static MappedColumn setCoupleSecondRow(String source, String dest) {
        return setCouple(source, dest, true);
    }

    public static MappedColumn setCouple(String source, String dest) {
        MappedColumn value = new MappedColumn();
        value.setSource(source);
        value.setDest(dest);
        return value;
    }

    public static MappedColumn fromSource(String source) {
        return setCouple(source, null);
    }

    public static MappedColumn forDest(String dest) {
        return setCouple(null, dest);
    }

    public static MappedColumn setDestStaticValue(String dest, String staticValue) {
        return new MappedColumn().setDest(dest).setStaticValue(staticValue);
    }

    public static MappedColumn setDestStaticValueSecondRow(String dest, String staticValue) {
        return setDestStaticValue(dest, staticValue).setSecondRow(true);
    }

    public static MappedColumn setDestFromCustomerField(String dest, String fieldFromCustomer) {
        return new MappedColumn().setDest(dest).setFieldFromCustomer(fieldFromCustomer);
    }

    public static MappedColumn setDestFieldFromCustomerSecondRow(String dest, String fieldFromCustomer) {
        return setDestFromCustomerField(dest, fieldFromCustomer).setSecondRow(true);
    }

    public Integer getSource() {
        return source;
    }

    public MappedColumn setSource(Integer source) {
        this.source = source;
        return this;
    }

    @JsonIgnore
    public MappedColumn setSource(String source) {
        return setSource(ColumnConverter.convert(source));
    }

    public Integer getDest() {
        return dest;
    }

    public MappedColumn setDest(Integer dest) {
        this.dest = dest;
        return this;
    }

    @JsonIgnore
    public MappedColumn setDest(String dest) {
        return setDest(ColumnConverter.convert(dest));
    }

    public String getSourceA() {
        return sourceA;
    }

    public void setSourceA(String sourceA) {
        this.sourceA = sourceA;
        setSource(sourceA);
    }

    public String getDestA() {
        return destA;
    }

    public void setDestA(String destA) {
        this.destA = destA;
        setDest(destA);
    }

    @JsonIgnore
    public String getDestAValue() {
        if (destA != null)
            return destA + (Util.has(secondRow) ? "2" : "1");
        return null;
    }

    @JsonIgnore
    public String getSourceAValue() {
        if (sourceA != null)
            return sourceA + (Util.has(secondRow) ? "2" : "1");
        return null;
    }

    @JsonIgnore
    public String getDestAPDFValue() {
        if (destA != null)
            return destAPDF + (Util.has(secondRow) ? "2" : "1");
        return null;
    }

    @JsonIgnore
    public String getSourceAPDFValue() {
        if (sourceAPDF != null)
            return sourceAPDF + (Util.has(secondRow) ? "2" : "1");
        return null;
    }

    public Integer getSourcePDF() {
        return sourcePDF;
    }

    public MappedColumn setSourcePDF(Integer sourcePDF) {
        this.sourcePDF = sourcePDF;
        return this;
    }

    @JsonIgnore
    public MappedColumn setSourcePDF(String sourcePDF) {
        return setSourcePDF(ColumnConverter.convert(sourcePDF));
    }

    public Integer getDestPDF() {
        return destPDF;
    }

    public MappedColumn setDestPDF(Integer destPDF) {
        this.destPDF = destPDF;
        return this;
    }

    @JsonIgnore
    public MappedColumn setDestPDF(String destPDF) {
        return setDestPDF(ColumnConverter.convert(destPDF));
    }

    @JsonIgnore
    public String getSourceAPDF() {
        return sourceAPDF;
    }

    @JsonIgnore
    public MappedColumn setSourceAPDF(String sourceAPDF) {
        this.sourceAPDF = sourceAPDF;
        return this;
    }

    public String getDestAPDF() {
        return destAPDF;
    }

    public MappedColumn setDestAPDF(String destAPDF) {
        this.destAPDF = destAPDF;
        return this;
    }

    @JsonIgnore
    public void parse() {

        if (Util.isNullOrNegative(dest))
            dest = null;
        if (Util.isNullOrNegative(source))
            source = null;
        if (Util.isNullOrNegative(destPDF))
            destPDF = null;
        if (Util.isNullOrNegative(sourcePDF))
            sourcePDF = null;


        if (Util.notEmpty(destA) && dest == null)
            setDest(destA);
        if (Util.notEmpty(sourceA) && source == null)
            setSource(sourceA);

        if (Util.notEmpty(destAPDF) && destPDF == null)
            setDestPDF(destAPDF);

        if (Util.notEmpty(sourceAPDF) && sourcePDF == null)
            setSourcePDF(sourceAPDF);


        if (Util.isEmpty(destA) && dest != null)
            destA = ColumnConverter.convert(dest);

        if (Util.isEmpty(sourceA) && source != null)
            sourceA = ColumnConverter.convert(source);

        if (Util.isEmpty(destAPDF) && destPDF != null)
            destAPDF = ColumnConverter.convert(destPDF);

        if (Util.isEmpty(sourceAPDF) && sourcePDF != null)
            sourceAPDF = ColumnConverter.convert(sourcePDF);

    }

    public Boolean isSecondRow() {
        return secondRow;
    }

    public Boolean isDate() {
        return date;
    }

    public String getSourceDateFormat() {
        return sourceDateFormat;
    }

    public MappedColumn setSourceDateFormat(String sourceDateFormat) {
        this.sourceDateFormat = sourceDateFormat;
        return this;
    }

    public String getDestDateFormat() {
        return destDateFormat;
    }

    public MappedColumn setDestDateFormat(String destDateFormat) {
        this.destDateFormat = destDateFormat;
        return this;
    }

    public Boolean isNumber() {
        return number;
    }

    public String getSourceNumberFormat() {
        return sourceNumberFormat;
    }

    public MappedColumn setSourceNumberFormat(String sourceNumberFormat) {
        this.sourceNumberFormat = sourceNumberFormat;
        return this;
    }

    public String getDestNumberFormat() {
        return destNumberFormat;
    }

    public MappedColumn setDestNumberFormat(String destNumberFormat) {
        this.destNumberFormat = destNumberFormat;
        return this;
    }

    public String getFieldFromCustomer() {
        return fieldFromCustomer;
    }

    public MappedColumn setFieldFromCustomer(String fieldFromCustomer) {
        this.fieldFromCustomer = fieldFromCustomer;
        return this;
    }

    public String getStaticValue() {
        return staticValue;
    }

    public MappedColumn setStaticValue(String staticValue) {
        this.staticValue = staticValue;
        return this;
    }

    public List<MappedColumn> getConcatenatedColumns() {
        return concatenatedColumns;
    }

    public MappedColumn setConcatenatedColumns(List<MappedColumn> concatenatedColumns) {
        this.concatenatedColumns = concatenatedColumns;
        return this;
    }

    @JsonIgnore
    public MappedColumn addConcatenatedColumn(MappedColumn mappedColumn) {
        if (concatenatedColumns == null)
            concatenatedColumns = new ArrayList<>();
        concatenatedColumns.add(mappedColumn);
        return this;
    }

    public Boolean getSecondRow() {
        return secondRow;
    }

    public MappedColumn setSecondRow(Boolean secondRow) {
        this.secondRow = secondRow;
        return this;
    }

    public Boolean getDate() {
        return date;
    }

    public MappedColumn setDate(Boolean date) {
        this.date = date;
        return this;
    }

    public Boolean getNumber() {
        return number;
    }

    public MappedColumn setNumber(Boolean number) {
        this.number = number;
        return this;
    }

    public String getConcatenator() {
        return concatenator;
    }

    public MappedColumn setConcatenator(String concatenator) {
        this.concatenator = concatenator;
        return this;
    }

    @JsonIgnore
    public int getType() {
        MappedColumn newValue = this;
        if (Util.has(newValue.getDate()))
            return MappedColumnTypes.DATE;
        if (Util.has(newValue.getNumber()))
            return MappedColumnTypes.NUMBER;
        if (Util.notEmpty(newValue.getFieldFromCustomer()))
            return MappedColumnTypes.FIELD;
        if (Util.notEmpty(newValue.getStaticValue()))
            return MappedColumnTypes.STATIC;
        if (Util.notEmpty(newValue.getConcatenatedColumns()))
            return MappedColumnTypes.CONCAT;
        return MappedColumnTypes.SIMPLE;
    }

    @JsonIgnore
    public String getTypeString() {
        String result = MappedColumnTypes.getTypeName(getType());
        if (this.getSourcePDF() != null && this.getDestPDF() != null) {
            result += "(" + ColumnConverter.convert(sourcePDF) + "->" + ColumnConverter.convert(destPDF) + ")";

        }
        return result;
    }

    @JsonIgnore
    public String getValue() {
        int type = getType();
        String result = "";
        switch (type) {
            case MappedColumnTypes.FIELD:
                result = getFieldFromCustomer();
                break;
            case MappedColumnTypes.STATIC:
                result = getStaticValue();
                break;
            case MappedColumnTypes.CONCAT: {
                StringBuilder sb = new StringBuilder();
                sb.append("Conc:'" + (concatenator == null ? "" : concatenator) + "' =>>");
                boolean seted = false;
                for (MappedColumn mappedColumn : getConcatenatedColumns()) {
                    mappedColumn.parse();
                    if (seted)
                        sb.append(",");
                    if (Util.notEmpty(mappedColumn.getSourceAValue()))
                        sb.append(mappedColumn.getSourceAValue());
                    sb.append((" " + mappedColumn.getValue()).trim());
                    seted = true;
                }
                result = sb.toString();
            }
            break;
            default:
                break;
        }
        return result;
    }
}
