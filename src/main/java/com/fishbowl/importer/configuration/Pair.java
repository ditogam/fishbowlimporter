package com.fishbowl.importer.configuration;


import java.util.Objects;

/**
 * Created by dito on 4/16/16.
 */
public class Pair<T, V> {
    private final T left;
    private final V right;

    public Pair(T left, V right) {
        this.left = left;
        this.right = right;
    }


    public T getLeft() {
        return left;
    }

    public V getRight() {
        return right;
    }

    @Override
    public String toString() {
        return right.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (left.equals(obj))
            return true;
        if(obj instanceof Object[])
        {
            Object[] objs=(Object[])obj;
            if(objs.length>0 && objs[0]!=null)
                return objs[0].toString().equals(left.toString());
        }
        if (!(obj instanceof Pair))
            return false;
        Pair<T, V> k = (Pair<T, V>) obj;
        return k.left.equals(this.left);
    }

    @Override
    public int hashCode() {
        return left.hashCode();
    }
}
