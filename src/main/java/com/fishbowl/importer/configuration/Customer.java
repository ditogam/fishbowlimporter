package com.fishbowl.importer.configuration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fishbowl.importer.utils.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 4/13/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Customer {


    @JsonProperty("id")
    private Long id;

    @JsonProperty("mapped_id")
    private Long mappedId;

    @CustomerFieldShow
    @JsonProperty("customer_name")
    @RequeredField("Cutomer name")
    private String customerName;

    @JsonProperty("first_row_header")
    private Boolean fistRowHeader;

    @CustomerFieldShow
    @JsonProperty("customer_address")
    @RequeredField("Cutomer Address")
    private String customerAddress;

    @CustomerFieldShow
    @JsonProperty("customer_city")
    @RequeredField("Cutomer City")
    private String customerCity;

    @CustomerFieldShow
    @JsonProperty("customer_state")
    @RequeredField("Cutomer State")
    private String customerState;

    @CustomerFieldShow
    @JsonProperty("customer_zip")
    @RequeredField("Cutomer ZipCode")
    private String customerZipCode;

    @CustomerFieldShow
    @JsonProperty("customer_country")
    @RequeredField("Cutomer Country")
    private String customerCountry = "United States";

    @JsonProperty("default_source_date_format")
    @RequeredField("Source date format")
    private String defaultSourceDateFormat = "M/d/yyyy HH:mm";

    @JsonProperty("default_dest_date_format")
    @RequeredField("Destination date format")
    private String defaultDestDateFormat = "M/d/yyyy";

    @JsonProperty("default_source_number_format")
    @RequeredField("Source number format")
    private String defaultSourceNumberFormat = "###,###.###";

    @JsonProperty("default_dest_number_format")
    @RequeredField("Destination date format")
    private String defaultDestNumberFormat = "###.##";


    @JsonProperty("file_download_options")
    private FileDownloadOptions fileDownloadOptions;


    @JsonIgnore
    private boolean twoRows = false;

    @JsonIgnore
    private int maxColumns;

    @JsonIgnore
    private boolean pdfColumns;

    @JsonIgnore
    private List<Pair<Integer, Integer>> pdfColumnsIndexes;

    @JsonProperty("duplicate_column")
    private MappedColumn duplicateColumn = MappedColumn.fromSource("U");

    @JsonProperty("same_columns")
    private List<MappedColumn> sameColumns = Arrays.asList(MappedColumn.fromSource("L"), MappedColumn.fromSource("M"));


    @JsonProperty("mapped_columns")
    private List<MappedColumn> mappedColumns = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public Customer setId(Long id) {
        this.id = id;
        return this;
    }

    public String getCustomerName() {
        return customerName;
    }

    public Customer setCustomerName(String customerName) {
        this.customerName = customerName;
        return this;
    }

    public Boolean getFistRowHeader() {
        return fistRowHeader;
    }

    public Customer setFistRowHeader(Boolean fistRowHeader) {
        this.fistRowHeader = fistRowHeader;
        return this;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public Customer setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
        return this;
    }

    public String getCustomerCity() {
        return customerCity;

    }

    public Customer setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
        return this;
    }

    public String getCustomerState() {
        return customerState;
    }

    public Customer setCustomerState(String customerState) {
        this.customerState = customerState;
        return this;
    }

    public String getCustomerZipCode() {
        return customerZipCode;
    }

    public Customer setCustomerZipCode(String customerZipCode) {
        this.customerZipCode = customerZipCode;
        return this;
    }

    public String getCustomerCountry() {
        return customerCountry;
    }

    public Customer setCustomerCountry(String customerCountry) {
        this.customerCountry = customerCountry;
        return this;
    }

    public String getDefaultSourceDateFormat() {
        return defaultSourceDateFormat;
    }

    public Customer setDefaultSourceDateFormat(String defaultSourceDateFormat) {
        this.defaultSourceDateFormat = defaultSourceDateFormat;
        return this;
    }

    public String getDefaultDestDateFormat() {
        return defaultDestDateFormat;
    }

    public Customer setDefaultDestDateFormat(String defaultDestDateFormat) {
        this.defaultDestDateFormat = defaultDestDateFormat;
        return this;
    }

    public String getDefaultSourceNumberFormat() {
        return defaultSourceNumberFormat;
    }

    public Customer setDefaultSourceNumberFormat(String defaultSourceNumberFormat) {
        this.defaultSourceNumberFormat = defaultSourceNumberFormat;
        return this;
    }

    public String getDefaultDestNumberFormat() {
        return defaultDestNumberFormat;
    }

    public Customer setDefaultDestNumberFormat(String defaultDestNumberFormat) {
        this.defaultDestNumberFormat = defaultDestNumberFormat;
        return this;
    }


    public FileDownloadOptions getFileDownloadOptions() {
        return fileDownloadOptions;
    }

    public Customer setFileDownloadOptions(FileDownloadOptions fileDownloadOptions) {
        this.fileDownloadOptions = fileDownloadOptions;
        return this;
    }

    public List<MappedColumn> getMappedColumns() {
        return mappedColumns;
    }

    public Customer setMappedColumns(List<MappedColumn> mappedColumns) {
        this.mappedColumns = mappedColumns;
        return this;
    }


    public MappedColumn getDuplicateColumn() {
        return duplicateColumn;
    }

    public void setDuplicateColumn(MappedColumn duplicateColumn) {
        this.duplicateColumn = duplicateColumn;
    }

    public List<MappedColumn> getSameColumns() {
        return sameColumns;
    }

    public void setSameColumns(List<MappedColumn> sameColumns) {
        this.sameColumns = sameColumns;
    }

    public Long getMappedId() {
        return mappedId;
    }

    public Customer setMappedId(Long mappedId) {
        this.mappedId = mappedId;
        return this;
    }

    public Customer addMappedColumn(MappedColumn mappedColumn) {
        if (mappedColumns == null)
            mappedColumns = new ArrayList<>();
        mappedColumns.add(mappedColumn);
        return this;
    }

    public void parse() {
        pdfColumns = false;
        pdfColumnsIndexes = new ArrayList<>();
        if (duplicateColumn != null)
            duplicateColumn.parse();
        if (sameColumns != null)
            sameColumns.stream().filter(sameColumn -> sameColumn != null).forEach(MappedColumn::parse);
        for (MappedColumn column : mappedColumns) {
            column.parse();
            if (Util.has(column.isSecondRow()))
                twoRows = true;
            if (column.getDest() != null) {
                maxColumns = Math.max(maxColumns, column.getDest());
            }

            if (column.getSourcePDF() != null && column.getDestPDF() != null) {
                pdfColumnsIndexes.add(new Pair<>(column.getSourcePDF(), column.getDestPDF()));
                pdfColumns = true;
            }
        }
        maxColumns++;
    }

    @JsonIgnore
    public boolean isPdfColumns() {
        return pdfColumns;
    }

    @JsonIgnore
    public List<Pair<Integer, Integer>> getPdfColumnsIndexes() {
        return pdfColumnsIndexes;
    }

    public boolean isTwoRows() {
        return twoRows;
    }

    public int getMaxColumns() {
        return maxColumns;
    }

    @Override
    public String toString() {
        return getCustomerName();
    }


    public void clearMainFields() {
        customerName = null;
        customerAddress = null;
        customerCity = null;
        customerState = null;
        customerZipCode = null;
    }


}
