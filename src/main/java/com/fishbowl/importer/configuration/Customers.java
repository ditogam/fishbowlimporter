package com.fishbowl.importer.configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 4/13/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Customers {
    @JsonProperty("customers")
    private List<Customer> customers = new ArrayList<>();

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }
}
