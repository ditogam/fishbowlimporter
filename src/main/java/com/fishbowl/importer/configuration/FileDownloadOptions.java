package com.fishbowl.importer.configuration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.http.entity.ContentType;
import org.springframework.http.HttpMethod;

/**
 * Created by dimitri.gamkrelidze on 4/15/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FileDownloadOptions {

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("user_password")
    private String userPassword;

    @JsonProperty("user_name_param_name")
    private String userNameParamName;

    @JsonProperty("user_password_param_name")
    private String userPasswordParamName;

    @JsonProperty("login_options")
    private HttpParams loginOptions;

    @JsonProperty("download_options")
    private HttpParams downloadOptions;


    public String getUserName() {
        return userName;
    }

    public FileDownloadOptions setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public FileDownloadOptions setUserPassword(String userPassword) {
        this.userPassword = userPassword;
        return this;
    }

    public String getUserNameParamName() {
        return userNameParamName;
    }

    public FileDownloadOptions setUserNameParamName(String userNameParamName) {
        this.userNameParamName = userNameParamName;
        return this;
    }

    public String getUserPasswordParamName() {
        return userPasswordParamName;
    }

    public FileDownloadOptions setUserPasswordParamName(String userPasswordParamName) {
        this.userPasswordParamName = userPasswordParamName;
        return this;
    }

    public HttpParams getLoginOptions() {
        return loginOptions;
    }

    public FileDownloadOptions setLoginOptions(HttpParams loginOptions) {
        this.loginOptions = loginOptions;
        return this;
    }

    public HttpParams getDownloadOptions() {
        return downloadOptions;
    }

    public FileDownloadOptions setDownloadOptions(HttpParams downloadOptions) {
        this.downloadOptions = downloadOptions;
        return this;
    }

    @JsonIgnore
    public HttpParams setUrl(String loginUrl, HttpParams httpParams) {
        httpParams = set(httpParams);
        httpParams.setUrl(loginUrl);
        return httpParams;
    }

    @JsonIgnore
    public HttpParams set(HttpParams httpParams) {
        if (httpParams == null)
            httpParams = new HttpParams();
        return httpParams;
    }

    @JsonIgnore
    public FileDownloadOptions setLoginUrl(String loginUrl) {
        loginOptions = setUrl(loginUrl, loginOptions);
        return this;
    }


    @JsonIgnore
    public FileDownloadOptions setDownloadUrl(String loginUrl) {
        downloadOptions = setUrl(loginUrl, downloadOptions);
        return this;
    }

    @JsonIgnore
    public HttpParams addParams(String key, String value, HttpParams httpParams) {
        httpParams = set(httpParams);
        httpParams.addParams(key, value);
        return httpParams;
    }


    @JsonIgnore
    public HttpParams addParams(String key, String value, Boolean requestBody, Boolean requestPart, HttpParams httpParams) {
        httpParams = set(httpParams);
        httpParams.addParams(key, value, requestBody, requestPart);
        return httpParams;
    }


    @JsonIgnore
    public FileDownloadOptions addDownloadParams(String key, String value) {
        downloadOptions = addParams(key, value, downloadOptions);
        return this;
    }

    @JsonIgnore
    public FileDownloadOptions addLoginParams(String key, String value) {
        loginOptions = addParams(key, value, loginOptions);
        return this;
    }

    @JsonIgnore
    public FileDownloadOptions addDownloadParams(String key, String value, Boolean requestBody, Boolean requestPart) {
        downloadOptions = addParams(key, value, requestBody, requestPart, downloadOptions);
        return this;
    }

    @JsonIgnore
    public FileDownloadOptions addLoginParams(String key, String value, Boolean requestBody, Boolean requestPart) {
        loginOptions = addParams(key, value, requestBody, requestPart, loginOptions);
        return this;
    }


    @JsonIgnore
    public HttpParams setMethod(HttpMethod method, HttpParams httpParams) {
        httpParams = set(httpParams);
        httpParams.setMethod(method.toString());
        return httpParams;
    }

    @JsonIgnore
    public FileDownloadOptions setLoginMethod(HttpMethod method) {
        loginOptions = setMethod(method, loginOptions);
        return this;
    }

    @JsonIgnore
    public FileDownloadOptions setDownloadMethod(HttpMethod method) {
        downloadOptions = setMethod(method, downloadOptions);
        return this;
    }

    @JsonIgnore
    public HttpParams setContentType(ContentType contentType, HttpParams httpParams) {
        httpParams = set(httpParams);
        httpParams.setMethod(contentType.toString());
        return httpParams;
    }

    @JsonIgnore
    public FileDownloadOptions setLoginContentType(ContentType contentTyp) {
        loginOptions = setContentType(contentTyp, loginOptions);
        return this;
    }

    @JsonIgnore
    public FileDownloadOptions setDownloadContentType(ContentType contentTyp) {
        downloadOptions = setContentType(contentTyp, downloadOptions);
        return this;
    }

}
