package com.fishbowl.importer.configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dimitri.gamkrelidze on 4/14/2016.
 */
public class MappedColumnTypes {

    public static final int SIMPLE = 0;
    public static final int DATE = 1;
    public static final int NUMBER = 2;
    public static final int STATIC = 3;
    public static final int FIELD = 4;
    public static final int CONCAT = 5;

    static Map<Integer, String> mpTypes = new HashMap<>();

    static {
        mpTypes.put(SIMPLE, "SIMPLE");
        mpTypes.put(DATE, "DATE");
        mpTypes.put(NUMBER, "NUMBER");
        mpTypes.put(STATIC, "STATIC");
        mpTypes.put(FIELD, "FIELD");
        mpTypes.put(CONCAT, "CONCAT");
    }

    public static final String getTypeName(int type) {
        return mpTypes.get(type);
    }

}
