package com.fishbowl.importer.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.configuration.Pair;
import com.fishbowl.importer.dao.CustomerDao;
import com.fishbowl.importer.entity.ECustomer;
import com.fishbowl.importer.utils.DBGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 4/20/2016.
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    protected CustomerDao customerDao;


    @Autowired
    private ApplicationContext applicationContext;

    private ObjectMapper om;


    @PostConstruct
    private void init() {
        om = new ObjectMapper();
    }

    @Override
    public void save(Customer t) throws Exception {
        String json = om.writeValueAsString(t);
        ECustomer eCustomer = null;
        if (t.getId() == null)
            eCustomer = new ECustomer();
        else
            eCustomer = customerDao.getCustomer(t.getId());
        if (eCustomer == null)
            eCustomer = new ECustomer();
        eCustomer.setJson(json);
        customerDao.save(eCustomer);
        t.setId(eCustomer.getId());
    }


    @Override
    public Customer getCustomer(Long id) throws Exception {
        ECustomer eCustomer = customerDao.getCustomer(id);
        return createCustomer(eCustomer);
    }

    public Customer createCustomer(ECustomer eCustomer) throws java.io.IOException {
        String json = eCustomer.getJson();
        return om.readValue(json, Customer.class);
    }

    @Override
    public List<Customer> getAll() throws Exception {
        List<Customer> customers = new ArrayList<>();
        List<ECustomer> eCustomers = customerDao.getAll();
        for (ECustomer eCustomer : eCustomers) {
            customers.add(createCustomer(eCustomer));
        }
        return customers;
    }

    @Override
    public void recreateDB() throws Exception {
        DBGenerator.initData(applicationContext);
    }

    @Override
    public List<String> unique_items(Long customer_id, List<String> ids){
        return customerDao.unique_items(customer_id, ids);
    }

    @Override
    public List<Pair<Long, String>> getAllFishblowCustomers() throws Exception {
        return customerDao.getAllFishblowCustomers();
    }
}
