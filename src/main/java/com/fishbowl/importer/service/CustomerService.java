package com.fishbowl.importer.service;

import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.configuration.Pair;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 4/20/2016.
 */
public interface CustomerService {
    public void save(Customer t) throws Exception;

    public Customer getCustomer(Long id) throws Exception;

    public List<Customer> getAll() throws Exception;

    public void recreateDB() throws Exception;

    public List<String> unique_items(Long customer_id,List<String> ids);

    public List<Pair<Long, String>> getAllFishblowCustomers() throws Exception;
}
