package com.fishbowl.importer;

import com.fishbowl.importer.utils.AdvancedEncryptionStandard;

/**
 * Created by dimitri.gamkrelidze on 4/21/2016.
 */
public class PasswordEncoder {

    private String decoded;

    public PasswordEncoder(String encoded) {
        this.decoded = encoded;
        if (encoded == null)
            return;
        if (encoded.startsWith(AdvancedEncryptionStandard.PREFIX) && encoded.endsWith(AdvancedEncryptionStandard.SUFFIX)) {
            encoded = encoded.substring(AdvancedEncryptionStandard.PREFIX.length());
            encoded = encoded.substring(0, encoded.length() - AdvancedEncryptionStandard.SUFFIX.length());
            try {
                decoded = new AdvancedEncryptionStandard().decrypt(encoded);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public String getDecoded() {
        return decoded;
    }
}
