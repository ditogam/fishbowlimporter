package com.fishbowl.importer.dao;

import com.fishbowl.importer.configuration.Pair;
import com.fishbowl.importer.entity.ECustomer;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 4/15/2016.
 */
@Component
public interface CustomerDao {
    public void save(ECustomer t) throws Exception;

    public List<ECustomer> getAll() throws Exception;

    public ECustomer getCustomer(Long id) throws Exception;

    public List<String> unique_items(Long customer_id, List<String> ids);

    public List<Pair<Long, String>> getAllFishblowCustomers() throws Exception;


}
