package com.fishbowl.importer.dao.impl;

import com.fishbowl.importer.configuration.Pair;
import com.fishbowl.importer.dao.CustomerDao;
import com.fishbowl.importer.entity.ECustomer;
import com.fishbowl.importer.utils.Util;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 4/15/2016.
 */
@Repository
public class CustomerDaoImpl extends AbstractEntityManager implements CustomerDao {
    @Override
    @Transactional
    public void save(ECustomer t) throws Exception {
        if (t.getId() == null)
            getEntityManager().persist(t);
        else
            getEntityManager().merge(t);
    }

    @Override
    @Transactional
    public List<ECustomer> getAll() throws Exception {
        return getEntityManager().createNamedQuery("ECustomer.findAll").getResultList();
    }

    @Override
    public ECustomer getCustomer(Long id) throws Exception {
        return getEntityManager().find(ECustomer.class, id);
    }

    private List<String> unique_items_private(Long customer_id, List<String> ids) {

        StringBuilder sql = new StringBuilder("select distinct CUSTOMERPO from OPENQUERY(FDATA, 'select * from SO where CUSTOMERID =" + customer_id
                + " and (");

        boolean started = false;

        for (String id : ids) {
            if (started)
                sql.append(" or ");
            sql.append("CUSTOMERPO||'' & '' like ''%");
            sql.append(id + " & %''");
            started = true;
        }

        sql.append(")') AS d");

        String sSql = sql.toString();
        List<String> result = getEntityManager().createNativeQuery(sSql).getResultList();
        return result;
    }

    @Override
    public List<String> unique_items(Long customer_id, List<String> ids) {
        if (customer_id == null || Util.isEmpty(ids))
            return new ArrayList<>();
        List<List<String>> splittedIds = Util.split(ids, 10);

        List<String> result = new ArrayList<>();
        for (List<String> splittedId : splittedIds) {
            result.addAll(unique_items_private(customer_id, splittedId));
        }
        return result;
    }

    @Override
    public List<Pair<Long, String>> getAllFishblowCustomers() throws Exception {
        String sql = "select ID,NAME from v_customer ORDER by NAME";
        List<?> list = getEntityManager().createNativeQuery(sql).getResultList();

        List<Pair<Long, String>> result = new ArrayList<>();
        if (Util.notEmpty(list)) {
            for (Object o : list) {
                Object[] objs = (Object[]) o;
                if (Util.notEmpty(objs) && objs[0] != null && objs[1] != null)
                    result.add(new Pair<>(Long.parseLong(objs[0].toString()), objs[1].toString()));
            }
        }
        return result;
    }
}
