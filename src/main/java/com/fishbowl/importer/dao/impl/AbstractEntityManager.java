package com.fishbowl.importer.dao.impl;

import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.jpa.HibernateEntityManager;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Connection;
import java.util.Map;

public abstract class AbstractEntityManager {

    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public <T> void createEntity(T t) {
        this.getEntityManager().persist(t);
    }

    public <T> void updateEntity(T t) {
        this.getEntityManager().merge(t);
    }

    public Connection getConnection() {
        HibernateEntityManager hem = (HibernateEntityManager) this.getEntityManager();
        SessionImplementor sim = (SessionImplementor) hem.getSession();
        return sim.connection();
    }

    protected Query createSearchQuery(String sql, int page, int count, Class<?> clazz, Map<String, Object> params) {
        Query q = null;
        if (clazz == null) {
            q = this.getEntityManager().createNativeQuery(sql);
        } else {
            q = this.getEntityManager().createNativeQuery(sql, clazz);
        }

        for (String key : params.keySet()) {
            q.setParameter(key, params.get(key));
        }

        if (count > 0) {
            q.setMaxResults(count);
            if (page > 0) {
                q.setFirstResult((page - 1) * count);
            }
        }

        return q;
    }
}
