package com.fishbowl.importer.entity;

import javax.persistence.*;

/**
 * Created by dimitri.gamkrelidze on 4/15/2016.
 */
@Entity
@Table(name = "customer")
@NamedQueries({@NamedQuery(name = "ECustomer.findAll", query = "SELECT t FROM ECustomer t order by id")})
public class ECustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id = null;

    @Column(name = "json")
    @Lob
    private String json;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
