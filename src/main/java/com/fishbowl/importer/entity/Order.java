package com.fishbowl.importer.entity;

import com.fishbowl.importer.configuration.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by dimitri.gamkrelidze on 5/13/2016.
 */
@Entity
@Table(name = "so_order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id = null;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "flag_header")
    @ConfigField(fieldName = "Flag SO", mainRow = true, column = "A")
    @StaticField("SO")
    private String flagHeader;

    @Column(name = "po_num")
    @ConfigField(fieldName = "PONum", mainRow = true, column = "U")
    @FromSourceField(required = true)
    private String pONum;

    @Column(name = "o_date")
    @ConfigField(fieldName = "Date", mainRow = true, column = "W")
    @FromSourceField()
    @DateField()
    private Date date;

    @Column(name = "flag_Item")
    @ConfigField(fieldName = "Flag Item", mainRow = false, column = "A")
    @StaticField("Item")
    private String flagItem;

    @Column(name = "SOItemTypeID")
    @ConfigField(fieldName = "SOItemTypeID", mainRow = false, column = "B")
    @MapField(mapMethodName = "getSOItemTypeID")
    private Long sOItemTypeID;

    @Column(name = "product_number")
    @ConfigField(fieldName = "ProductNumber", mainRow = true, column = "C")
    @FromSourceField()
    private String productNumber;


    @Column(name = "vendor_po_num")
    @ConfigField(fieldName = "VendorPONum", mainRow = true, column = "V")
    @FromSourceField(required = true)
    private String vendorPONum;


    @Column(name = "so_number")
    @ConfigField(fieldName = "SONum", mainRow = true, column = "B", group = "From Customer")
    @StaticField()
    private String soNumber;

    @Column(name = "status")
    @ConfigField(fieldName = "Status", mainRow = true, column = "C", group = "From Customer")
    @MapField(mapMethodName = "getStatuses")
    private Long status;

    @Column(name = "customer_name")
    @ConfigField(fieldName = "CustomerName", mainRow = true, column = "D", group = "From Customer")
    @FromField("customerName")
    private String customerName;

    @Column(name = "customer_contact")
    @ConfigField(fieldName = "CustomerContact", mainRow = true, column = "E", group = "From Customer")
    @FromField("customerName")
    private String customerContact;

    @Column(name = "bill_to_name")
    @ConfigField(fieldName = "BillToName", mainRow = true, column = "F", group = "From Customer")
    @FromField("customerName")
    private String billToName;

    @Column(name = "bill_to_address")
    @ConfigField(fieldName = "BillToAddress", mainRow = true, column = "G", group = "From Customer")
    @FromField("customerAddress")
    private String billToAddress;

    @Column(name = "bill_to_city")
    @ConfigField(fieldName = "BillToCity", mainRow = true, column = "H", group = "From Customer")
    @FromField("customerCity")
    private String billToCity;

    @Column(name = "bill_to_state")
    @ConfigField(fieldName = "BillToState", mainRow = true, column = "I", group = "From Customer")
    @FromField("customerState")
    private String billToState;

    @Column(name = "bill_to_zip")
    @ConfigField(fieldName = "BillToZip", mainRow = true, column = "J", group = "From Customer")
    @FromField("customerZipCode")
    private String billToZip;

    @Column(name = "bill_to_country")
    @ConfigField(fieldName = "BillToCountry", mainRow = true, column = "K", group = "From Customer")
    @FromField("customerCountry")
    private String billToCountry;


    @Column(name = "ship_to_name")
    @ConfigField(fieldName = "ShipToName", mainRow = true, column = "L", group = "Shipping")
    @FromSourceField
    private String shipToName;

    @Column(name = "ship_to_address")
    @ConfigField(fieldName = "ShipToAddress", mainRow = true, column = "M", group = "Shipping")
    @FromSourceField
    private String shipToAddress;

    @Column(name = "ship_to_city")
    @ConfigField(fieldName = "ShipToCity", mainRow = true, column = "N", group = "Shipping")
    @FromSourceField
    private String shipToCity;

    @Column(name = "ship_to_state")
    @ConfigField(fieldName = "ShipToState", mainRow = true, column = "O", group = "Shipping")
    @FromSourceField
    private String shipToState;

    @Column(name = "ship_to_zip")
    @ConfigField(fieldName = "ShipToZip", mainRow = true, column = "P", group = "Shipping")
    @FromSourceField
    private String shipToZip;

    @Column(name = "ship_to_country")
    @ConfigField(fieldName = "ShipToCountry", mainRow = true, column = "Q", group = "Shipping")
    @FromSourceField(staticValue = "United States")
    private String shipToCountry;

    @Column(name = "carrier_name")
    @ConfigField(fieldName = "CarrierName", mainRow = true, column = "R", group = "Shipping")
    @FromSourceField
    private String carrierName;

    @Column(name = "tax_rate_name")
    @ConfigField(fieldName = "TaxRateName", mainRow = true, column = "S", group = "Shipping")
    @FromSourceField(staticValue = "None")
    private String taxRateName;

    @Column(name = "priority_id")
    @ConfigField(fieldName = "PriorityId", mainRow = true, column = "T", group = "Shipping")
    @MapField(mapMethodName = "getPriorities")
    private Long priorityId;


}
