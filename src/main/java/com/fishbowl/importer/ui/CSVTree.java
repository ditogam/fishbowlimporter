package com.fishbowl.importer.ui;

import com.fishbowl.importer.converter.CustomerRowConverter;
import com.fishbowl.importer.converter.RowComparator;
import com.fishbowl.importer.ui.classes.CSVProperties;
import com.fishbowl.importer.utils.ColumnConverter;
import com.fishbowl.importer.utils.Util;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dito on 5/13/16.
 */
public class CSVTree extends TreeTableView<CSVProperties> {
    private TreeItem<CSVProperties> root;
    private int max_columns;
    private MenuItem save;


    public void setScene(Scene scene) {
        String file = getClass().getClassLoader().getResource("forms/styled-tree-table.css").toExternalForm();
        scene.getStylesheets().add(file);
    }

    public CSVTree(CustomerRowConverter converter, boolean editable) {
        super(new TreeItem<>());


        root = this.getRoot();
        setShowRoot(false);
        this.setEditable(editable);
        max_columns = converter.getMax_columns();
        for (int i = 0; i < max_columns; i++) {
            addColumn(ColumnConverter.convert(i), "val" + i, editable);
        }

        TreeItem<CSVProperties> myroot = root;
        converter.refresh();
        for (RowComparator rowComparator : converter.getRowComparators()) {
            List<List<String>> list = rowComparator.getChild_rows();
            if (Util.isEmpty(list))
                continue;
            TreeItem<CSVProperties> parent = createItem(myroot, rowComparator.getFirst_row(), rowComparator.isDuplicated());
//            parent.expandedProperty().addListener(observable -> {
//                refresh();
//            });
            for (List<String> item : list) {
                createItem(parent, item, false);
            }
        }
        this.setColumnResizePolicy(TreeTableView.UNCONSTRAINED_RESIZE_POLICY);
        if (editable) {
            setRowFactory(new Callback<TreeTableView<CSVProperties>, TreeTableRow<CSVProperties>>() {
                @Override
                public TreeTableRow<CSVProperties> call(TreeTableView<CSVProperties> param) {
                    return new TreeTableRow<CSVProperties>() {
                        @Override
                        protected void updateItem(CSVProperties item, boolean empty) {
                            super.updateItem(item, empty);
                            ObservableList<String> styleClass = getStyleClass();
                            if (item != null && item.getDuplicate()) {
                                styleClass.add("highlight");
                            } else
                                styleClass.removeAll(Collections.singleton("highlight"));

                        }
                    };
                }
            });
            final ContextMenu contextMenu = new ContextMenu();
            MenuItem delete = new MenuItem("Delete");
            save = new MenuItem("Save");

            contextMenu.getItems().addAll(delete, save);
            delete.setOnAction(event -> deleteRow());

            this.setContextMenu(contextMenu);
        }
    }

    public void setHandler(EventHandler<ActionEvent> handler) {
        save.setOnAction(handler);
    }

    public List<List<String>> getItems() {
        List<List<String>> result = new ArrayList<>();
        for (TreeItem<CSVProperties> treeItem : getRoot().getChildren()) {
            CSVProperties properties = treeItem.getValue();
            result.add(properties.getValues(max_columns));
            for (TreeItem<CSVProperties> item : treeItem.getChildren()) {
                properties = item.getValue();
                result.add(properties.getValues(max_columns));
            }
        }
        return result;
    }

    private void deleteRow() {
        final TreeItem<CSVProperties> item = getSelectionModel().getSelectedItem();
        if (item == null)
            return;
        TreeItem<CSVProperties> parent = item.getParent();
        if (parent == null)
            return;
        String message = "";

        if ((parent.getValue() == null && item.getValue().getDuplicate()) || (parent.getValue() != null && parent.getValue().getDuplicate()))
            message = "Do you want to delete duplicated row with it's sub items? ";
        else
            message = "The row is not duplicated, do you want to delete row? ";
        if (UI.ask("Attention", "", message)) {
            parent.getChildren().remove(item);
            if (parent.getChildren().isEmpty())
                parent.getParent().getChildren().remove(parent);
            refresh();
        }
    }

    public void filterChanged(String filter) {
        if (filter.isEmpty()) {
            setRoot(root);
        } else {
            filter = filter.toLowerCase();
            TreeItem<CSVProperties> filteredRoot = new TreeItem<>();
            filter(root, filter, filteredRoot);
            setRoot(filteredRoot);
            refresh();
        }
    }

    private boolean isMatch(CSVProperties value, String filter) {
        return value.match(filter);//values().stream().anyMatch(o -> o.getValue().contains(filter));
    }

    private void filter(TreeItem<CSVProperties> root, String filter, TreeItem<CSVProperties> filteredRoot) {

        for (TreeItem<CSVProperties> child : root.getChildren()) {
            TreeItem<CSVProperties> filteredChild = new TreeItem<>();
            filteredChild.setValue(child.getValue());
            filteredChild.setExpanded(true);
            filter(child, filter, filteredChild);
            if (!filteredChild.getChildren().isEmpty() || isMatch(filteredChild.getValue(), filter)) {
                filteredRoot.getChildren().add(filteredChild);
            }
        }
    }

    private TreeItem<CSVProperties> createItem(TreeItem<CSVProperties> parent, List<String> items, boolean duplicate) {
        TreeItem<CSVProperties> item = new TreeItem<>();
        CSVProperties value = new CSVProperties(duplicate, items);
        item.setValue(value);
        parent.getChildren().add(item);
        item.setExpanded(true);
        return item;
    }

    protected void addColumn(String label, String dataIndex, boolean editable) {
        TreeTableColumn<CSVProperties, String> column = new TreeTableColumn<>(label);
        if (dataIndex.equals("0")) {
            column.setPrefWidth(150);
        } else {
            column.setEditable(editable);
            column.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());
        }
        column.setCellValueFactory(new TreeItemPropertyValueFactory<CSVProperties, String>(dataIndex));
        getColumns().add(column);
    }
}
