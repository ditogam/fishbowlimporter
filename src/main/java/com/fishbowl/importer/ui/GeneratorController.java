package com.fishbowl.importer.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.configuration.FileDownloadOptions;
import com.fishbowl.importer.configuration.Pair;
import com.fishbowl.importer.converter.CSVCreator;
import com.fishbowl.importer.converter.CSVEventListener;
import com.fishbowl.importer.converter.CustomerRowConverter;
import com.fishbowl.importer.service.CustomerService;
import com.fishbowl.importer.ui.utils.TableUtils;
import com.fishbowl.importer.utils.CLI;
import com.fishbowl.importer.utils.ColumnConverter;
import com.fishbowl.importer.utils.Downloader;
import com.fishbowl.importer.utils.Util;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.MapValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.net.URL;
import java.util.*;
import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 4/15/2016.
 */
public class GeneratorController extends Application implements CSVEventListener, Initializable {
    public static Stage primaryStage;
    private static ApplicationContext applicationContext;
    @FXML
    BorderPane rootPane;
    @FXML
    Button btGenerate;
    @FXML
    Button btOpen;
    @FXML
    ComboBox<Customer> cbCustomer;
    @FXML
    Label lblSourceFile;
    @FXML
    Label lblOutput;
    @FXML
    ProgressBar pbProgress;
    @FXML
    Button btnDownload;
    @FXML
    TextField ftLoginLink;
    @FXML
    TextField ftDownloadLink;
    @FXML
    TextField ftUserName;
    @FXML
    TextField pfPassword;
    @FXML
    Label lblSourceOriginFile;
    @FXML
    Label lblSourcePDF;
    @FXML
    Label lblOutputOrigin;
    @FXML
    Label lblOutputPdf;
    @FXML
    GridPane paneSourceFile;
    @FXML
    GridPane panePdfFile;
    @FXML
    GridPane paneDestFile;

    private CustomerService customerService;

    public static void main(String[] args) throws Exception {
//        Calendar c=new GregorianCalendar();
//        c.set(Calendar.HOUR_OF_DAY,18);
//        TimeZone tz1 = TimeZone.getDefault();
//        TimeZone tz2 = TimeZone.getTimeZone("America/New_York");
//        long timeDifference = tz1.getRawOffset() - tz2.getRawOffset() + tz1.getDSTSavings() - tz2.getDSTSavings();
//        System.out.println(new Date(System.currentTimeMillis()-timeDifference));
//        System.out.println(new Date(c.getTimeInMillis()-timeDifference));
//        c.set(Calendar.HOUR_OF_DAY,23);
//        System.out.println(new Date(c.getTimeInMillis()-timeDifference));
        CLI.usePosixParser(args);
        launch(args);

    }

    private void acceptFile(Pane pane, final Label label) {
        pane.setOnDragOver(event -> {
            Dragboard db = event.getDragboard();
            if (db.hasFiles()) {
                event.acceptTransferModes(TransferMode.COPY);
            } else {
                event.consume();
            }
        });

        // Dropping over surface
        pane.setOnDragDropped(event -> {
            Dragboard db = event.getDragboard();
            boolean success = false;
            if (db.hasFiles()) {
                success = true;
                String filePath = null;
                for (File file : db.getFiles()) {
                    filePath = file.getAbsolutePath();
                    label.setText(filePath);
                }
            }
            event.setDropCompleted(success);
            event.consume();
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObjectMapper om = new ObjectMapper();
        om.enable(SerializationFeature.INDENT_OUTPUT);

        acceptFile(paneSourceFile, lblSourceFile);
        acceptFile(panePdfFile, lblOutputPdf);
        acceptFile(paneDestFile, lblOutput);


        setOpenFile(lblSourceFile);
        setOpenFile(lblOutputPdf);
        setOpenFile(lblOutput);

        cbCustomer.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            newValue.parse();
            ftLoginLink.setText(null);
            ftDownloadLink.setText(null);
            ftUserName.setText(null);
            pfPassword.setText(null);
            if (newValue == null)
                return;
            newValue.parse();
            panePdfFile.setDisable(!Util.has(newValue.isPdfColumns()));
            if (newValue.getFileDownloadOptions() == null)
                return;
            FileDownloadOptions o = newValue.getFileDownloadOptions();
            ftLoginLink.setText(o.getLoginOptions() == null ? null : o.getLoginOptions().getUrl());
            ftDownloadLink.setText(o.getDownloadOptions() == null ? null : o.getDownloadOptions().getUrl());
            ftUserName.setText(o.getUserName());
            pfPassword.setText(o.getUserPassword());


        });

        customerService = applicationContext.getBean(CustomerService.class);

        setCustomers();

    }

    public void setCustomers() {
        try {
            cbCustomer.setItems(FXCollections.observableArrayList(customerService.getAll()));
            if (!cbCustomer.getItems().isEmpty())
                cbCustomer.getSelectionModel().select(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOpenFile(final Label lblSourceFile) {
        lblSourceFile.setCursor(Cursor.HAND);
        lblSourceFile.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                try {
                    File file = new File(lblSourceFile.getText());
                    Desktop.getDesktop().open(file);
                } catch (Throwable e) {
                    UI.error("Error", "Error", "", e);
                    e.printStackTrace();
                }

            }
        });
    }

    private File openFile(String title, Label label, boolean askIfExists) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        if (label != null && label.getText() != null) {
            try {
                fileChooser.setInitialFileName(label.getText());
            } catch (Exception e) {

            }
        }
        File file = askIfExists ? fileChooser.showSaveDialog(UI.primaryStage) : fileChooser.showOpenDialog(UI.primaryStage);
        if (file != null) {
            if (!askIfExists) {
                if (!file.exists() || !file.isFile()) {
                    UI.warning("Please choose existent File", "Attention", "");
                    return null;
                }
            }
            if (label != null)
                label.setText(file.getAbsolutePath());
            return file;
        }
        return null;
    }

    @FXML
    public void chooseSource(ActionEvent event) {
        openFile("Choose Source File", lblSourceFile, false);
    }

    @FXML
    public void chooseDestination(ActionEvent event) {
        openFile("Choose Output File", lblOutput, true);
    }

    @FXML
    public void chooseSourceOrigin(ActionEvent event) {
        openFile("Choose Source File", lblSourceOriginFile, false);
    }

    @FXML
    public void chooseSourcePDFOrigin(ActionEvent event) {
        openFile("Choose Source File PDF", lblSourcePDF, false);
    }

    @FXML
    public void chooseDestinationOrigin(ActionEvent event) {
        openFile("Choose Output File", lblOutputOrigin, true);
    }

    @FXML
    public void concat(ActionEvent event) {
        openFile("Choose Output File", lblOutput, true);
    }

    @FXML
    public void choosePDFCSV(ActionEvent event) {
        openFile("Choose Source PDF/CSV File", lblOutputPdf, false);
    }

    @FXML
    public void generate(ActionEvent event) {

        pbProgress.progressProperty().unbind();
        pbProgress.setProgress(0);
        Customer customer = cbCustomer.getValue();
        CustomerRowConverter converter = null;
        try {
            converter = new CustomerRowConverter(customer);
        } catch (Exception e) {
            UI.error("Error", "Error", "", e);
            e.printStackTrace();
        }
        final String outputFileName = createOutputFile(customer);
        if (outputFileName == null)
            return;
        final CustomerRowConverter rowConverter = converter;
        CSVTask task = new CSVTask(lblSourceFile.getText(), lblOutputPdf.getText(), converter, customer);
        pbProgress.progressProperty().bind(task.progressProperty());
        task.setOnSucceeded(event1 -> setOutputFile(outputFileName, rowConverter));
        task.setOnFailed(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                reenable();
            }
        });
        rootPane.setDisable(true);
        new Thread(task).start();

    }

    private void setOutputFile(String outputFileName, CustomerRowConverter rowConverter) {
        reenable();
        lblOutput.setText(outputFileName);
        List<String> unique_items = customerService.unique_items(rowConverter.getCustomer().getMappedId(), rowConverter.getIds());
//        unique_items.clear();
//        unique_items.add("3949926");
//        unique_items.add("3962729");
        Pair<Parent, CSVTree> pair = createTreeTableFromRecords(rowConverter, true, unique_items, outputFileName);
        Parent outTableView = pair.getLeft();

        Stage dialogStage = new Stage();

//        dialogStage.setFullScreen(true);
//        dialogStage.setScene(UI.myScene);

        dialogStage.setTitle("Revie results");

        dialogStage.setResizable(true);
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        Scene scene = new Scene(outTableView, screenBounds.getWidth(), screenBounds.getHeight());



        dialogStage.setScene(scene);
        pair.getRight().setScene(dialogStage.getScene());
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(UI.primaryStage.getScene().getWindow());
            dialogStage.setMaximized(true);

        dialogStage.showAndWait();


        reenable();
    }

    private String createOutputFile(Customer value) {

        try {
            File user_dir = new File(System.getProperty("user.dir"));
            File output_dir = new File(user_dir, "output");
            File customer_dir = new File(output_dir, value.getCustomerName().trim());
            File date_dir = new File(customer_dir, Util.getDateFormat("yyyy-MM-dd").format(new Date()));
            if (!date_dir.exists() && !date_dir.mkdirs())
                throw new Exception("Output Directory cannot be created");
            File output_file = new File(date_dir, Util.getDateFormat("HHmmss").format(new Date()) + ".csv");
            if (!output_file.createNewFile())
                throw new Exception("Output File cannot be created");
            output_file.delete();
            return output_file.getAbsolutePath();
        } catch (Throwable e) {
            UI.error("Error", "Error", "", e);
            e.printStackTrace();
        }


        return null;
    }

    private void generateDataInMap(ObservableList<Map<String, String>> allData, String[] record) {
        Map<String, String> dataRow = new HashMap<>();
        for (int i = 0; i < record.length; i++) {
            dataRow.put(i + "", record[i]);
        }
        allData.add(dataRow);

    }


    private Pair<Parent, CSVTree> createTreeTableFromRecords(CustomerRowConverter converter, boolean editable, List<String> unique_items, final String outputFileName) {
        converter.checkDuplicated(unique_items);

        final CSVTree tv = new CSVTree(converter, editable);
        BorderPane pane = new BorderPane(tv);
        TextField tf = new TextField();
        BorderPane filterPane = new BorderPane(tf);
        filterPane.setLeft(new Label("Filter"));
        if (editable) {
            final ProgressBar pbar = new ProgressBar();
            pbar.progressProperty().unbind();
            EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        pbar.progressProperty().unbind();
                        pbar.setProgress(0);
                        Task task = new Task<Integer>() {
                            @Override
                            protected Integer call() throws Exception {
                                CSVCreator creator = new CSVCreator();
                                creator.addEventListeners(new CSVEventListener() {
                                    @Override
                                    public void proceed(int size, int currentPos) {
                                        updateProgress(currentPos, size);
                                    }
                                })
                                        .save(outputFileName, tv.getItems());
                                return null;
                            }
                        };
                        pbar.progressProperty().bind(task.progressProperty());
                        task.setOnSucceeded(event1 -> UI.inform("Succeed", "", "Saved to file:\n" + outputFileName));
                        task.setOnFailed(event1 -> pbar.progressProperty().unbind());
                        new Thread(task).start();

                    } catch (Exception e) {
                        UI.error("Error", "Error", "", e);
                        e.printStackTrace();
                    }
                }
            };
            Button save = new Button("Save");
            filterPane.setRight(save);

            AnchorPane ap = new AnchorPane(pbar);
            UI.anchor0(pbar);
            pane.setBottom(new BorderPane(ap));
            save.setOnAction(event);
            tv.setHandler(event);

        }

        tf.textProperty().addListener((observable, oldValue, newValue) -> tv.filterChanged(newValue));
        pane.setTop(filterPane);
        TableUtils.installCopyPasteHandler(tv);
        return new Pair<>(pane, tv);
    }

    private Node createTableFromRecords(List<String[]> records) {

        int maxColumns = Integer.MIN_VALUE;
        for (String[] record : records) {
            maxColumns = Math.max(record.length, maxColumns);
        }
        List<TableColumn<Map, String>> columns = new ArrayList<>();
        Callback<TableColumn<Map, String>, TableCell<Map, String>>
                cellFactoryForMap = (TableColumn<Map, String> p) ->
                new TextFieldTableCell(new StringConverter() {
                    @Override
                    public String toString(Object t) {
                        return t == null ? null : t.toString();
                    }

                    @Override
                    public Object fromString(String string) {
                        return string;
                    }
                });
        TableColumn numberCol = new TableColumn("Index");
        numberCol.setEditable(false);
        numberCol.setCellFactory(new Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn p) {
                return new TableCell() {
                    @Override
                    public void updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        setGraphic(null);
                        setText(empty ? null : getIndex() + 1 + "");
                    }
                };
            }
        });
        columns.add(numberCol);
        for (int i = 0; i < maxColumns; i++) {
            TableColumn<Map, String> column = new TableColumn<>(ColumnConverter.convert(i));
            column.setEditable(false);
            column.setCellValueFactory(new MapValueFactory<>(i + ""));

            columns.add(column);
            column.setCellFactory(cellFactoryForMap);
        }
        ObservableList<Map<String, String>> allData = FXCollections.observableArrayList();
        for (String[] record : records) {
            generateDataInMap(allData, record);
        }
        FilteredList<Map<String, String>> filteredData = new FilteredList<>(allData, p -> true);
        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Map<String, String>> sortedData = new SortedList<>(filteredData);


        TableView tv = new TableView(sortedData);
        tv.getColumns().addAll(columns);
        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tv.comparatorProperty());
        TableUtils.installCopyPasteHandler(tv);
        BorderPane pane = new BorderPane(tv);
        TextField tf = new TextField();
        BorderPane filterPane = new BorderPane(tf);
        filterPane.setLeft(new Label("Filter"));

        pane.setTop(filterPane);


        tf.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(map -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every map with filter text.
                String lowerCaseFilter = newValue.toLowerCase().trim();
                for (String key : map.keySet()) {
                    if (map.get(key).toLowerCase().trim().contains(lowerCaseFilter))
                        return true;
                }

                return false; // Does not match.
            });
        });
        return pane;
    }

    private List<String[]> csv2List(List<CSVRecord> records) {
        List<String[]> result = new ArrayList<>();
        for (CSVRecord record : records) {
            String[] data = new String[record.size()];
            for (int i = 0; i < data.length; i++) {
                data[i] = record.get(i);
            }
            result.add(data);
        }
        return result;
    }

    private List<String[]> csv2List(Map<Integer, Map<String, String>> map) {
        List<String[]> result = new ArrayList<>();
        for (Integer key : map.keySet()) {
            for (String valuekey : map.get(key).keySet()) {
                String[] data = new String[]{ColumnConverter.convert(key), valuekey, map.get(key).get(valuekey)};
                result.add(data);
            }

        }

        return result;
    }

    @FXML
    public void preview(ActionEvent event) {
        Integer maxRecords = 10;
        List<CSVRecord> inputRecords = null;
        Map<Integer, Map<Integer, Map<String, String>>> mapping = null;
        CustomerRowConverter converter = null;
        try {
            converter = new CustomerRowConverter(cbCustomer.getValue());
        } catch (Exception e) {
            UI.error("Error", "Error", "", e);
            e.printStackTrace();
        }
        CSVTask task = new CSVTask(lblSourceFile.getText(), lblOutputPdf.getText(), converter, converter.getCustomer());
        try {
            mapping = task.createStreams(maxRecords);
            try (FileReader fr = new FileReader(lblSourceFile.getText()); CSVParser sourceParser = new CSVParser(fr, CSVFormat.DEFAULT);
            ) {
                inputRecords = Util.getCsvRecordsRowNum(maxRecords + 1, sourceParser);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (inputRecords != null) {
            Node inTableView = createTableFromRecords(csv2List(inputRecords));
            Node outTableView = createTreeTableFromRecords(converter, false, new ArrayList<>(), "").getLeft();//createTableFromRecords(csv2List(outputRecords));
            Accordion accordion = null;
            if (mapping != null && !mapping.isEmpty()) {
                accordion = new Accordion();
                List<TitledPane> panes = new ArrayList<>();
                for (Integer key : mapping.keySet()) {
                    TitledPane pane = new TitledPane("Mapping for column " + ColumnConverter.convert(key), createTableFromRecords(csv2List(mapping.get(key))));
                    makeBold(pane);
                    panes.add(pane);
                }
                accordion.getPanes().addAll(panes);
                accordion.setExpandedPane(panes.get(0));
            }


            Stage dialogStage = new Stage();
            dialogStage.setTitle("Preview result");

            dialogStage.setResizable(true);


            VBox vBox = new VBox(createBoldLabel("Source data"), inTableView, createBoldLabel("Result data"), outTableView);
            if (accordion != null)
                vBox.getChildren().addAll(createBoldLabel("Mapping data"), accordion);


            dialogStage.setScene(new Scene(vBox));

            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(((Node) event.getSource()).getScene().getWindow());
//            dialogStage.setMaximized(true);
            dialogStage.showAndWait();


        }

    }

    public Labeled createBoldLabel(String labelText) {
        Labeled label = new Label(labelText);
        makeBold(label);
        return label;
    }

    public void makeBold(Labeled label) {
        Font font = label.getFont();
        font = Font.font(font.getName(), FontWeight.BOLD, font.getSize() + 2);
        label.setFont(font);
    }

    private void reenable() {
        rootPane.setDisable(false);
        pbProgress.progressProperty().unbind();
    }

    @FXML
    public void open(ActionEvent event) {
        try {
            Desktop.getDesktop().open(new File(lblOutput.getText()));
        } catch (Throwable e) {
            UI.error("Error", "Error", "", e);
            e.printStackTrace();
        }
    }

    @FXML
    public void download(ActionEvent event) {
        File openFile = openFile("Choose Output File", null, true);
        if (openFile == null)
            return;
        try (FileOutputStream os = new FileOutputStream(openFile)) {
            Downloader.download(cbCustomer.getValue().getFileDownloadOptions(), os, ftUserName.getText(), pfPassword.getText());
        } catch (Throwable e) {
            UI.error("Error", "Error", "", e);
            e.printStackTrace();
            return;
        }
        lblSourceFile.setText(openFile.getAbsolutePath());
    }

    @FXML
    public void launchEditor(ActionEvent event) {
        Stage stage = new Stage();
        stage.setTitle("Editor");
        try {
            final Pair<Scene, CustomerDesignController> pair = UI.crateateEditor(stage, customerService);
            stage.showAndWait();
            if (pair.getRight().isChanged())
                setCustomers();
        } catch (Throwable e) {
            UI.error("Error", "Error", "", e);
            e.printStackTrace();
        }
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("/spring/root-context.xml");
        FXMLLoader loader = UI.getLoader("forms/file_generator.fxml");
        Scene scene = new Scene(loader.load());
        primaryStage.setTitle("fishbowl Loader");
        primaryStage.setScene(scene);

        primaryStage.setMaximized(true);
        primaryStage.show();
        UI.primaryStage = primaryStage;
        UI.myScene=scene;
    }

    @Override
    public void proceed(int size, int currentPos) {
        pbProgress.setProgress(currentPos);
    }
}
