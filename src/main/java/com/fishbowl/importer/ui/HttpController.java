package com.fishbowl.importer.ui;

import com.fishbowl.importer.configuration.HttpParams;
import com.fishbowl.importer.configuration.Pair;
import com.fishbowl.importer.utils.Util;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import org.apache.http.entity.ContentType;
import org.springframework.http.HttpMethod;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by dimitri.gamkrelidze on 4/18/2016.
 */
public class HttpController implements Initializable {
    @FXML
    private BorderPane rootPane;

    @FXML
    private TextField tfUrl;

    @FXML
    private ComboBox<String> cbHttpMethod;

    @FXML
    private ComboBox<String> cbContentType;

    @FXML
    private Pane bpHeaders;

    @FXML
    private Pane bpParams;
    @FXML
    private Pane fgMain;


    private PropertyEditorController headers;
    private PropertyEditorController params;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        headers = createController(bpHeaders, true);
        params = createController(bpParams, false);
        setValues(cbHttpMethod, Util.generateEnumValues(HttpMethod.class));
        setValues(cbContentType, Util.generateStaticValues(ContentType.class));

        setContentType(null);
        setMethod(null);
    }

    public void setContentType(String value) {
        cbContentType.setValue(Util.isEmpty(value) ? ContentType.APPLICATION_JSON.toString() : value.trim());
    }

    public void setMethod(String value) {
        cbHttpMethod.setValue(Util.isEmpty(value) ? HttpMethod.GET.toString() : value.trim());
    }

    public void clearData() {
        headers.clearData();
        params.clearData();
        tfUrl.setText(null);
        setContentType(null);
        setMethod(null);
    }

    public HttpParams getData() {
        HttpParams httpParams = new HttpParams();
        httpParams.setUrl(Util.getStrValue(tfUrl.getText()));
        httpParams.setContentType(Util.getStrValue(cbContentType.getValue()));
        httpParams.setMethod(Util.getStrValue(cbHttpMethod.getValue()));
        httpParams.setHeaders(headers.getDataMap());
        httpParams.setParams(params.getData());
        return httpParams;
    }

    public void setData(HttpParams httpParams) {
        clearData();
        if (httpParams == null)
            return;
        headers.setData(httpParams.getHeaders());
        params.setData(httpParams.getParams());
        tfUrl.setText(httpParams.getUrl());
        setMethod(httpParams.getMethod());
        setContentType(httpParams.getContentType());
    }

    private void setValues(ComboBox<String> combobox, List<String> list) {
        if (Util.isEmpty(list))
            return;
        combobox.setItems(FXCollections.observableArrayList(list));
        combobox.getSelectionModel().select(0);
    }

    private PropertyEditorController createController(Pane pane, boolean simple) {
        try {
            Pair<Region, PropertyEditorController> loader = UI.loadWithController("forms/property_editor.fxml", PropertyEditorController.class);
            UI.anchor0(loader.getLeft());
            pane.getChildren().addAll(loader.getLeft());
            loader.getRight().setType(simple);
            return loader.getRight();

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void enableDisableEditing(boolean enable) throws Exception {
        fgMain.setDisable(!enable);
        headers.enableDisableEditing(enable);
        params.enableDisableEditing(enable);
    }
}
