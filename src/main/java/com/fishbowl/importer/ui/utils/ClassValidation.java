package com.fishbowl.importer.ui.utils;

import com.fishbowl.importer.configuration.RequeredField;
import com.fishbowl.importer.utils.Util;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by dimitri.gamkrelidze on 4/19/2016.
 */
public class ClassValidation {


    public ClassValidation(Node node, Field field, Object object) throws FieldValidatorException {
        Object value = null;
        if (node instanceof TextField)
            value = ((TextField) node).getText();
        if (node instanceof CheckBox)
            value = ((CheckBox) node).isSelected();
        if (value instanceof String && value != null && Util.isEmpty(value.toString()))
            value = null;


        RequeredField requered = field.getAnnotation(RequeredField.class);
        if (requered != null && value == null) {
            node.requestFocus();
            throw new FieldValidatorException("Requered " + requered.value(), node);
        }
        if (value instanceof Boolean && value != null && !Util.has((Boolean) value))
            value = null;
        if (value == null)
            return;
        try {

            String fieldName = field.getName();
            fieldName = (fieldName.charAt(0) + "").toUpperCase() + fieldName.substring(1);

//            BeanUtils.populate(object, values);
            Method method = object.getClass().getDeclaredMethod("set" + fieldName, value.getClass());
            if (method != null)
                method.invoke(object, value);
        } catch (Exception e) {
            throw new FieldValidatorException(e, node);
        }
    }

    public static void validate(Object object, Scene scene) throws FieldValidatorException {
        for (Field field : object.getClass().getDeclaredFields()) {
            Node node = scene.lookup("#v" + field.getName());
            if (node != null)
                new ClassValidation(node, field, object);
        }
    }
}
