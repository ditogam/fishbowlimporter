package com.fishbowl.importer.ui.utils;

import javafx.scene.Node;

/**
 * Created by dimitri.gamkrelidze on 4/19/2016.
 */
public class FieldValidatorException extends Exception {
    private Node node;

    public FieldValidatorException(String message, Node node) {
        super(message);
        this.node = node;
    }

    public FieldValidatorException(Throwable cause, Node node) {
        super(cause);
        this.node = node;
    }

    public Node getNode() {
        return node;
    }
}
