package com.fishbowl.importer.ui.classes;

/**
 * Created by dimitri.gamkrelidze on 4/19/2016.
 */
public interface Validator {
    void validate();
}
