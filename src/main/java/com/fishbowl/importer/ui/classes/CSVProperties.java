package com.fishbowl.importer.ui.classes;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dito on 5/14/16.
 */
public class CSVProperties {


//    public static void main(String[] args){
//        new CSVProperties(Arrays.asList("sdf","dddd","ddd"));
//    }

    public CSVProperties(boolean duplicate, List<String> values) {
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < values.size(); i++) {
            map.put("val" + i, values.get(i));
        }
        try {
            BeanUtils.populate(this, map);
            System.out.println();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        setDuplicate(duplicate);

    }

    public List<String> getValues(int max_columns) {
        List<String> result = new ArrayList<>();
        try {
            Map<String, String> map = BeanUtils.describe(this);
            for (int i = 0; i < max_columns; i++) {
                String val = map.get("val" + i);
                if (val == null)
                    val = "";
                result.add(val);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    private final BooleanProperty duplicate = new SimpleBooleanProperty();
    private final List<StringProperty> values = new ArrayList<>();


    private StringProperty createProperty() {
        StringProperty p = new SimpleStringProperty();
        values.add(p);
        return p;
    }

    private final StringProperty val0 = createProperty();
    private final StringProperty val1 = createProperty();
    private final StringProperty val2 = createProperty();
    private final StringProperty val3 = createProperty();
    private final StringProperty val4 = createProperty();
    private final StringProperty val5 = createProperty();
    private final StringProperty val6 = createProperty();
    private final StringProperty val7 = createProperty();
    private final StringProperty val8 = createProperty();
    private final StringProperty val9 = createProperty();
    private final StringProperty val10 = createProperty();
    private final StringProperty val11 = createProperty();
    private final StringProperty val12 = createProperty();
    private final StringProperty val13 = createProperty();
    private final StringProperty val14 = createProperty();
    private final StringProperty val15 = createProperty();
    private final StringProperty val16 = createProperty();
    private final StringProperty val17 = createProperty();
    private final StringProperty val18 = createProperty();
    private final StringProperty val19 = createProperty();
    private final StringProperty val20 = createProperty();
    private final StringProperty val21 = createProperty();
    private final StringProperty val22 = createProperty();
    private final StringProperty val23 = createProperty();
    private final StringProperty val24 = createProperty();
    private final StringProperty val25 = createProperty();
    private final StringProperty val26 = createProperty();
    private final StringProperty val27 = createProperty();
    private final StringProperty val28 = createProperty();
    private final StringProperty val29 = createProperty();
    private final StringProperty val30 = createProperty();
    private final StringProperty val31 = createProperty();
    private final StringProperty val32 = createProperty();
    private final StringProperty val33 = createProperty();
    private final StringProperty val34 = createProperty();
    private final StringProperty val35 = createProperty();
    private final StringProperty val36 = createProperty();
    private final StringProperty val37 = createProperty();
    private final StringProperty val38 = createProperty();
    private final StringProperty val39 = createProperty();
    private final StringProperty val40 = createProperty();
    private final StringProperty val41 = createProperty();
    private final StringProperty val42 = createProperty();
    private final StringProperty val43 = createProperty();
    private final StringProperty val44 = createProperty();
    private final StringProperty val45 = createProperty();
    private final StringProperty val46 = createProperty();
    private final StringProperty val47 = createProperty();
    private final StringProperty val48 = createProperty();
    private final StringProperty val49 = createProperty();
    private final StringProperty val50 = createProperty();
    private final StringProperty val51 = createProperty();
    private final StringProperty val52 = createProperty();
    private final StringProperty val53 = createProperty();
    private final StringProperty val54 = createProperty();
    private final StringProperty val55 = createProperty();
    private final StringProperty val56 = createProperty();
    private final StringProperty val57 = createProperty();


    public boolean getDuplicate() {
        return duplicate.get();
    }

    public BooleanProperty duplicateProperty() {
        return duplicate;
    }

    public void setDuplicate(boolean duplicate) {
        this.duplicate.set(duplicate);
    }

    public String getVal0() {
        return val0.get();
    }

    public StringProperty val0Property() {
        return val0;
    }

    public void setVal0(String val0) {
        this.val0.set(val0);
    }

    public String getVal1() {
        return val1.get();
    }

    public StringProperty val1Property() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1.set(val1);
    }

    public String getVal2() {
        return val2.get();
    }

    public StringProperty val2Property() {
        return val2;
    }

    public void setVal2(String val2) {
        this.val2.set(val2);
    }

    public String getVal3() {
        return val3.get();
    }

    public StringProperty val3Property() {
        return val3;
    }

    public void setVal3(String val3) {
        this.val3.set(val3);
    }

    public String getVal4() {
        return val4.get();
    }

    public StringProperty val4Property() {
        return val4;
    }

    public void setVal4(String val4) {
        this.val4.set(val4);
    }

    public String getVal5() {
        return val5.get();
    }

    public StringProperty val5Property() {
        return val5;
    }

    public void setVal5(String val5) {
        this.val5.set(val5);
    }

    public String getVal6() {
        return val6.get();
    }

    public StringProperty val6Property() {
        return val6;
    }

    public void setVal6(String val6) {
        this.val6.set(val6);
    }

    public String getVal7() {
        return val7.get();
    }

    public StringProperty val7Property() {
        return val7;
    }

    public void setVal7(String val7) {
        this.val7.set(val7);
    }

    public String getVal8() {
        return val8.get();
    }

    public StringProperty val8Property() {
        return val8;
    }

    public void setVal8(String val8) {
        this.val8.set(val8);
    }

    public String getVal9() {
        return val9.get();
    }

    public StringProperty val9Property() {
        return val9;
    }

    public void setVal9(String val9) {
        this.val9.set(val9);
    }

    public String getVal10() {
        return val10.get();
    }

    public StringProperty val10Property() {
        return val10;
    }

    public void setVal10(String val10) {
        this.val10.set(val10);
    }

    public String getVal11() {
        return val11.get();
    }

    public StringProperty val11Property() {
        return val11;
    }

    public void setVal11(String val11) {
        this.val11.set(val11);
    }

    public String getVal12() {
        return val12.get();
    }

    public StringProperty val12Property() {
        return val12;
    }

    public void setVal12(String val12) {
        this.val12.set(val12);
    }

    public String getVal13() {
        return val13.get();
    }

    public StringProperty val13Property() {
        return val13;
    }

    public void setVal13(String val13) {
        this.val13.set(val13);
    }

    public String getVal14() {
        return val14.get();
    }

    public StringProperty val14Property() {
        return val14;
    }

    public void setVal14(String val14) {
        this.val14.set(val14);
    }

    public String getVal15() {
        return val15.get();
    }

    public StringProperty val15Property() {
        return val15;
    }

    public void setVal15(String val15) {
        this.val15.set(val15);
    }

    public String getVal16() {
        return val16.get();
    }

    public StringProperty val16Property() {
        return val16;
    }

    public void setVal16(String val16) {
        this.val16.set(val16);
    }

    public String getVal17() {
        return val17.get();
    }

    public StringProperty val17Property() {
        return val17;
    }

    public void setVal17(String val17) {
        this.val17.set(val17);
    }

    public String getVal18() {
        return val18.get();
    }

    public StringProperty val18Property() {
        return val18;
    }

    public void setVal18(String val18) {
        this.val18.set(val18);
    }

    public String getVal19() {
        return val19.get();
    }

    public StringProperty val19Property() {
        return val19;
    }

    public void setVal19(String val19) {
        this.val19.set(val19);
    }

    public String getVal20() {
        return val20.get();
    }

    public StringProperty val20Property() {
        return val20;
    }

    public void setVal20(String val20) {
        this.val20.set(val20);
    }

    public String getVal21() {
        return val21.get();
    }

    public StringProperty val21Property() {
        return val21;
    }

    public void setVal21(String val21) {
        this.val21.set(val21);
    }

    public String getVal22() {
        return val22.get();
    }

    public StringProperty val22Property() {
        return val22;
    }

    public void setVal22(String val22) {
        this.val22.set(val22);
    }

    public String getVal23() {
        return val23.get();
    }

    public StringProperty val23Property() {
        return val23;
    }

    public void setVal23(String val23) {
        this.val23.set(val23);
    }

    public String getVal24() {
        return val24.get();
    }

    public StringProperty val24Property() {
        return val24;
    }

    public void setVal24(String val24) {
        this.val24.set(val24);
    }

    public String getVal25() {
        return val25.get();
    }

    public StringProperty val25Property() {
        return val25;
    }

    public void setVal25(String val25) {
        this.val25.set(val25);
    }

    public String getVal26() {
        return val26.get();
    }

    public StringProperty val26Property() {
        return val26;
    }

    public void setVal26(String val26) {
        this.val26.set(val26);
    }

    public String getVal27() {
        return val27.get();
    }

    public StringProperty val27Property() {
        return val27;
    }

    public void setVal27(String val27) {
        this.val27.set(val27);
    }

    public String getVal28() {
        return val28.get();
    }

    public StringProperty val28Property() {
        return val28;
    }

    public void setVal28(String val28) {
        this.val28.set(val28);
    }

    public String getVal29() {
        return val29.get();
    }

    public StringProperty val29Property() {
        return val29;
    }

    public void setVal29(String val29) {
        this.val29.set(val29);
    }

    public String getVal30() {
        return val30.get();
    }

    public StringProperty val30Property() {
        return val30;
    }

    public void setVal30(String val30) {
        this.val30.set(val30);
    }

    public String getVal31() {
        return val31.get();
    }

    public StringProperty val31Property() {
        return val31;
    }

    public void setVal31(String val31) {
        this.val31.set(val31);
    }

    public String getVal32() {
        return val32.get();
    }

    public StringProperty val32Property() {
        return val32;
    }

    public void setVal32(String val32) {
        this.val32.set(val32);
    }

    public String getVal33() {
        return val33.get();
    }

    public StringProperty val33Property() {
        return val33;
    }

    public void setVal33(String val33) {
        this.val33.set(val33);
    }

    public String getVal34() {
        return val34.get();
    }

    public StringProperty val34Property() {
        return val34;
    }

    public void setVal34(String val34) {
        this.val34.set(val34);
    }

    public String getVal35() {
        return val35.get();
    }

    public StringProperty val35Property() {
        return val35;
    }

    public void setVal35(String val35) {
        this.val35.set(val35);
    }

    public String getVal36() {
        return val36.get();
    }

    public StringProperty val36Property() {
        return val36;
    }

    public void setVal36(String val36) {
        this.val36.set(val36);
    }

    public String getVal37() {
        return val37.get();
    }

    public StringProperty val37Property() {
        return val37;
    }

    public void setVal37(String val37) {
        this.val37.set(val37);
    }

    public String getVal38() {
        return val38.get();
    }

    public StringProperty val38Property() {
        return val38;
    }

    public void setVal38(String val38) {
        this.val38.set(val38);
    }

    public String getVal39() {
        return val39.get();
    }

    public StringProperty val39Property() {
        return val39;
    }

    public void setVal39(String val39) {
        this.val39.set(val39);
    }

    public String getVal40() {
        return val40.get();
    }

    public StringProperty val40Property() {
        return val40;
    }

    public void setVal40(String val40) {
        this.val40.set(val40);
    }

    public String getVal41() {
        return val41.get();
    }

    public StringProperty val41Property() {
        return val41;
    }

    public void setVal41(String val41) {
        this.val41.set(val41);
    }

    public String getVal42() {
        return val42.get();
    }

    public StringProperty val42Property() {
        return val42;
    }

    public void setVal42(String val42) {
        this.val42.set(val42);
    }

    public String getVal43() {
        return val43.get();
    }

    public StringProperty val43Property() {
        return val43;
    }

    public void setVal43(String val43) {
        this.val43.set(val43);
    }

    public String getVal44() {
        return val44.get();
    }

    public StringProperty val44Property() {
        return val44;
    }

    public void setVal44(String val44) {
        this.val44.set(val44);
    }

    public String getVal45() {
        return val45.get();
    }

    public StringProperty val45Property() {
        return val45;
    }

    public void setVal45(String val45) {
        this.val45.set(val45);
    }

    public String getVal46() {
        return val46.get();
    }

    public StringProperty val46Property() {
        return val46;
    }

    public void setVal46(String val46) {
        this.val46.set(val46);
    }

    public String getVal47() {
        return val47.get();
    }

    public StringProperty val47Property() {
        return val47;
    }

    public void setVal47(String val47) {
        this.val47.set(val47);
    }

    public String getVal48() {
        return val48.get();
    }

    public StringProperty val48Property() {
        return val48;
    }

    public void setVal48(String val48) {
        this.val48.set(val48);
    }

    public String getVal49() {
        return val49.get();
    }

    public StringProperty val49Property() {
        return val49;
    }

    public void setVal49(String val49) {
        this.val49.set(val49);
    }

    public String getVal50() {
        return val50.get();
    }

    public StringProperty val50Property() {
        return val50;
    }

    public void setVal50(String val50) {
        this.val50.set(val50);
    }

    public String getVal51() {
        return val51.get();
    }

    public StringProperty val51Property() {
        return val51;
    }

    public void setVal51(String val51) {
        this.val51.set(val51);
    }

    public String getVal52() {
        return val52.get();
    }

    public StringProperty val52Property() {
        return val52;
    }

    public void setVal52(String val52) {
        this.val52.set(val52);
    }

    public String getVal53() {
        return val53.get();
    }

    public StringProperty val53Property() {
        return val53;
    }

    public void setVal53(String val53) {
        this.val53.set(val53);
    }

    public String getVal54() {
        return val54.get();
    }

    public StringProperty val54Property() {
        return val54;
    }

    public void setVal54(String val54) {
        this.val54.set(val54);
    }

    public String getVal55() {
        return val55.get();
    }

    public StringProperty val55Property() {
        return val55;
    }

    public void setVal55(String val55) {
        this.val55.set(val55);
    }

    public String getVal56() {
        return val56.get();
    }

    public StringProperty val56Property() {
        return val56;
    }

    public void setVal56(String val56) {
        this.val56.set(val56);
    }

    public String getVal57() {
        return val57.get();
    }

    public StringProperty val57Property() {
        return val57;
    }

    public void setVal57(String val57) {
        this.val57.set(val57);
    }


    public boolean match(String filter) {
        for (StringProperty value : values) {
            String val = value.getValue();
            if (val != null && val.toLowerCase().contains(filter))
                return true;

        }
        return false;
    }
}
