package com.fishbowl.importer.ui.classes;

import com.fishbowl.importer.configuration.Customer;

import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 4/20/2016.
 */
public class CustomerWrapper {
    private Customer customer;

    public CustomerWrapper(Customer customer) {
        this.customer = customer;
    }

    public static CustomerWrapper[] wrap(List<Customer> customers) {
        CustomerWrapper[] result = new CustomerWrapper[customers.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = new CustomerWrapper(customers.get(i));
        }
        return result;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return customer == null ? super.toString() : customer.toString();
    }
}
