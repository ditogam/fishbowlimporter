package com.fishbowl.importer.ui.classes;

import com.fishbowl.importer.utils.Util;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by dimitri.gamkrelidze on 4/18/2016.
 */
public class PropertyField {
    private final SimpleStringProperty tcKey;
    private final SimpleStringProperty tcValue;
    private final SimpleBooleanProperty tcRequestBody;
    private final SimpleBooleanProperty tcRequestPart;

    public PropertyField(String key, String value, Boolean requestBody, Boolean requestPart) {
        this.tcKey = new SimpleStringProperty(key);
        this.tcValue = new SimpleStringProperty(value);
        this.tcRequestBody = new SimpleBooleanProperty(Util.has(requestBody));
        this.tcRequestPart = new SimpleBooleanProperty(Util.has(requestPart));
    }

    public PropertyField(String key, String value) {
        this(key, value, null, null);
    }

    public String getTcKey() {
        return tcKey.get();
    }

    public void setTcKey(String tcKey) {
        this.tcKey.set(tcKey);
    }

    public SimpleStringProperty tcKeyProperty() {
        return tcKey;
    }

    public String getTcValue() {
        return tcValue.get();
    }

    public void setTcValue(String tcValue) {
        this.tcValue.set(tcValue);
    }

    public SimpleStringProperty tcValueProperty() {
        return tcValue;
    }

    public boolean getTcRequestBody() {
        return tcRequestBody.get();
    }

    public void setTcRequestBody(boolean tcRequestBody) {
        this.tcRequestBody.set(tcRequestBody);
    }

    public SimpleBooleanProperty tcRequestBodyProperty() {
        return tcRequestBody;
    }

    public boolean getTcRequestPart() {
        return tcRequestPart.get();
    }

    public void setTcRequestPart(boolean tcRequestPart) {
        this.tcRequestPart.set(tcRequestPart);
    }

    public SimpleBooleanProperty tcRequestPartProperty() {
        return tcRequestPart;
    }
}
