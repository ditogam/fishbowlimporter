package com.fishbowl.importer.ui.classes;

import com.fishbowl.importer.configuration.MappedColumn;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by dimitri.gamkrelidze on 4/14/2016.
 */
public class MappedFieldProperty {
    private final SimpleStringProperty tcType;
    private final SimpleStringProperty tcValue;

    private final SimpleStringProperty tcSource;
    private final SimpleStringProperty tcDest;


    private MappedColumn mappedColumn;

    public MappedFieldProperty(MappedColumn mappedColumn) {
        mappedColumn.parse();
        this.mappedColumn = mappedColumn;
        tcSource = new SimpleStringProperty(mappedColumn.getSourceA());
        tcDest = new SimpleStringProperty(mappedColumn.getDestAValue());

        tcType = new SimpleStringProperty(mappedColumn.getTypeString());
        tcValue = new SimpleStringProperty(mappedColumn.getValue());
    }

    public void setNewValue(MappedColumn mappedColumn) {
        mappedColumn.parse();
        this.mappedColumn = mappedColumn;
        refresh();
    }

    public void refresh() {
        mappedColumn.parse();
        tcSource.setValue(mappedColumn.getSourceA());
        tcDest.setValue(mappedColumn.getDestAValue());
        tcType.setValue(mappedColumn.getTypeString());
        tcValue.setValue(mappedColumn.getValue());
    }


    public String getTcSource() {
        return tcSource.get();
    }

    public void setTcSource(String tcSource) {
        this.tcSource.set(tcSource);
    }

    public SimpleStringProperty tcSourceProperty() {
        return tcSource;
    }

    public String getTcDest() {
        return tcDest.get();
    }

    public void setTcDest(String tcDest) {
        this.tcDest.set(tcDest);
    }

    public SimpleStringProperty tcDestProperty() {
        return tcDest;
    }

    public String getTcType() {
        return tcType.get();
    }

    public void setTcType(String tcType) {
        this.tcType.set(tcType);
    }

    public SimpleStringProperty tcTypeProperty() {
        return tcType;
    }

    public String getTcValue() {
        return tcValue.get();
    }

    public void setTcValue(String tcValue) {
        this.tcValue.set(tcValue);
    }

    public SimpleStringProperty tcValueProperty() {
        return tcValue;
    }

    public MappedColumn getMappedColumn() {
        return mappedColumn;
    }
}
