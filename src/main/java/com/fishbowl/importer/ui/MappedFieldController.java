package com.fishbowl.importer.ui;

import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.configuration.CustomerFieldShow;
import com.fishbowl.importer.configuration.MappedColumn;
import com.fishbowl.importer.configuration.MappedColumnTypes;
import com.fishbowl.importer.ui.classes.MappedFieldProperty;
import com.fishbowl.importer.utils.ColumnConverter;
import com.fishbowl.importer.utils.Util;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;

/**
 * Created by dimitri.gamkrelidze on 4/14/2016.
 */
public class MappedFieldController implements Initializable {
    @FXML
    ToggleGroup radioGroup1;
    @FXML
    RadioButton rbSimple;
    @FXML
    RadioButton rbDate;
    @FXML
    RadioButton rbNumber;
    @FXML
    RadioButton rbStatic;
    @FXML
    RadioButton rbField;
    @FXML
    RadioButton rbConcat;


    @FXML
    GridPane grDateFormat;

    @FXML
    GridPane grNumberFormat;

    @FXML
    GridPane grCustomerFields;

    @FXML
    BorderPane bpStaticPane;

    @FXML
    BorderPane bpConcatPane;


    @FXML
    BorderPane pSource;

    @FXML
    BorderPane pDest;


    @FXML
    VBox vbContainer;
    @FXML
    ComboBox<String> cbCustomerFields;

    @FXML
    ComboBox<String> cbSource;
    @FXML
    ComboBox<String> cbDest;


    @FXML
    ComboBox<String> cbPDFDest;

    @FXML
    ComboBox<String> cbPDFSource;


    @FXML
    CheckBox cbSecondRow;


    @FXML
    TextField tStaticValue;

    @FXML
    TextField vdefaultSourceDateFormat;

    @FXML
    TextField vdefaultDestDateFormat;

    @FXML
    TextField vdefaultSourceNumberFormat;

    @FXML
    TextField vdefaultDestNumberFormat;


    @FXML
    TextField tfConcatenator;


    @FXML
    AnchorPane tablePlace;
    List<RadioButton> sourceNeeded = null;
    List<RadioButton> pdfNeeded = null;
    private MapedColumnTable mapedColumnTable;
    private Map<Toggle, List<Node>> showHideControls = new HashMap<>();
    private List<Node> allControlsToView = new ArrayList<>();
    private MappedFieldProperty newValue;

    @FXML
    private BorderPane pDestPDF;


    @FXML
    private BorderPane pSourcePDF;

    private void setShowHideControls(Toggle toggle) {
        try {
            pSource.setVisible(!(toggle.equals(rbConcat) || toggle.equals(rbStatic) || toggle.equals(rbField)));
            vbContainer.getChildren().removeAll(allControlsToView.toArray(new Node[0]));
            if (toggle == null)
                return;
            List<Node> nodes = showHideControls.get(toggle);
            if (nodes == null)
                return;
            vbContainer.getChildren().addAll(nodes.toArray(new Node[0]));
        } finally {


        }
    }


    public void setSimple() {
//        rbField.setVisible(false);
        rbConcat.setVisible(false);
        pDest.setVisible(false);
        cbSecondRow.setVisible(false);
        pDestPDF.setVisible(false);
        pSourcePDF.setVisible(false);

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        allControlsToView.addAll(Arrays.asList(grDateFormat, grNumberFormat, grCustomerFields, bpStaticPane, bpConcatPane));

        sourceNeeded = Arrays.asList(rbSimple, rbDate, rbNumber);
        pdfNeeded = new ArrayList<>(sourceNeeded);

        showHideControls.put(rbDate, Arrays.asList(grDateFormat));
        showHideControls.put(rbNumber, Arrays.asList(grNumberFormat));
        showHideControls.put(rbStatic, Arrays.asList(bpStaticPane));
        showHideControls.put(rbField, Arrays.asList(grCustomerFields));
        showHideControls.put(rbConcat, Arrays.asList(bpConcatPane));


        cbCustomerFields.getItems().add("");

        for (Field field : Customer.class.getDeclaredFields()) {
            CustomerFieldShow type = field.getDeclaredAnnotation(CustomerFieldShow.class);
            if (type != null)
                cbCustomerFields.getItems().add(field.getName());
        }

        cbDest.getItems().addAll(ColumnConverter.getAZ(true));
        cbSource.getItems().addAll(ColumnConverter.getAZ(true));

        cbPDFSource.getItems().addAll(ColumnConverter.getAZ(true));
        cbPDFDest.getItems().addAll(ColumnConverter.getAZ(true));

        setShowHideControls(radioGroup1.getSelectedToggle());
        tfConcatenator.setStyle("-fx-text-fill: green;-fx-control-inner-background: orange;");
        radioGroup1.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                setShowHideControls(newValue);
            }
        });

        try {
            mapedColumnTable = MapedColumnTable.createTable(true);
            UI.anchor0(mapedColumnTable.getTblMapedColumns());
            tablePlace.getChildren().addAll(mapedColumnTable.getTblMapedColumns());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearFields() {
        newValue = null;
        cbSource.setValue(null);
        cbDest.setValue(null);
        cbCustomerFields.setValue(null);
        radioGroup1.selectToggle(rbSimple);
        cbSecondRow.setSelected(false);
        tStaticValue.setText(null);
        vdefaultSourceDateFormat.setText(null);
        vdefaultDestDateFormat.setText(null);
        vdefaultSourceNumberFormat.setText(null);
        vdefaultDestNumberFormat.setText(null);
        tfConcatenator.setText(null);
        cbPDFSource.setValue(null);
        cbPDFDest.setValue(null);
        mapedColumnTable.clear();
    }

    public MappedColumn validate() throws Exception {
        MappedColumn column = new MappedColumn();

        boolean sourceSelected = false;
        for (RadioButton radioButton : sourceNeeded) {
            if (radioButton.isSelected()) {
                sourceSelected = true;
            }
        }
        if (sourceSelected && Util.isEmpty(cbSource.getValue())) {
            cbSource.requestFocus();
            throw new Exception("Select source");
        }
        if (sourceSelected)
            column.setSourceA(cbSource.getValue());


        if (pDest.isVisible()) {
            if (Util.isEmpty(cbDest.getValue())) {
                cbDest.requestFocus();
                throw new Exception("Select destination");
            }
            column.setDestA(cbDest.getValue());
            if (cbSecondRow.isSelected())
                column.setSecondRow(true);
        }
        String pdfSource = cbPDFSource.getValue();
        String pdfDest = cbPDFDest.getValue();

        boolean pdfSelected = false;
        for (RadioButton radioButton : pdfNeeded) {
            if (radioButton.isSelected()) {
                pdfSelected = true;
            }
        }

        if (pdfSelected && (Util.notEmpty(pdfSource) || Util.notEmpty(pdfDest))) {
            if (Util.isEmpty(pdfSource)) {
                cbPDFSource.requestFocus();
                throw new Exception("Select pdf source");
            }
            if (Util.isEmpty(pdfDest)) {
                cbPDFDest.requestFocus();
                throw new Exception("Select pdf destination");
            }
            column.setSourceAPDF(pdfSource.trim());
            column.setDestAPDF(pdfDest);
        }

        if (rbField.isSelected()) {
            if (Util.isEmpty(cbCustomerFields.getValue())) {
                cbCustomerFields.requestFocus();
                throw new Exception("Select Customer field");
            }
            column.setFieldFromCustomer(cbCustomerFields.getValue().trim());
        }

        if (rbStatic.isSelected()) {
            if (tStaticValue.getText() == null && tStaticValue.getText().isEmpty()) {
                tStaticValue.requestFocus();
                throw new Exception("Set static value");
            }
            column.setStaticValue(tStaticValue.getText());
        }

        if (rbConcat.isSelected()) {
            column.setConcatenatedColumns(mapedColumnTable.validate());
            column.setConcatenator(tfConcatenator.getText());
        }
        if (rbDate.isSelected())
            column.setDate(true);
        if (rbNumber.isSelected())
            column.setNumber(true);
        return column;
    }

    public void setMappedColumn(MappedFieldProperty column) {
        clearFields();
        if (column == null)
            return;
        this.newValue = column;

        MappedColumn newValue = column.getMappedColumn();


        cbSource.setValue(newValue.getSourceA());
        cbDest.setValue(newValue.getDestA());

        cbPDFSource.setValue(newValue.getSourceAPDF());
        cbPDFDest.setValue(newValue.getDestAPDF());


        radioGroup1.selectToggle(rbSimple);

        int type = newValue.getType();
        switch (type) {
            case MappedColumnTypes.FIELD:
                radioGroup1.selectToggle(rbField);
                break;
            case MappedColumnTypes.STATIC:
                radioGroup1.selectToggle(rbStatic);
                break;
            case MappedColumnTypes.CONCAT:
                radioGroup1.selectToggle(rbConcat);
                break;
            case MappedColumnTypes.DATE:
                radioGroup1.selectToggle(rbDate);
                break;
            case MappedColumnTypes.NUMBER:
                radioGroup1.selectToggle(rbNumber);
                break;
            default:
                break;
        }


        cbSecondRow.setSelected(Util.has(newValue.getSecondRow()));
        cbCustomerFields.setValue(newValue.getFieldFromCustomer());

        tStaticValue.setText(newValue.getStaticValue());

        vdefaultSourceDateFormat.setText(newValue.getSourceDateFormat());
        vdefaultDestDateFormat.setText(newValue.getDestDateFormat());
        vdefaultSourceNumberFormat.setText(newValue.getSourceNumberFormat());
        vdefaultDestNumberFormat.setText(newValue.getDestNumberFormat());

        mapedColumnTable.setItems(newValue.getConcatenatedColumns());
        tfConcatenator.setText(newValue.getConcatenator());

    }
}
