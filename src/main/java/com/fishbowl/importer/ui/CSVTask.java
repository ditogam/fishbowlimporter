package com.fishbowl.importer.ui;

import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.converter.CSVCreator;
import com.fishbowl.importer.converter.CSVEventListener;
import com.fishbowl.importer.converter.CustomerRowConverter;
import com.fishbowl.importer.utils.Util;
import javafx.concurrent.Task;

import java.io.*;
import java.util.Map;

/**
 * Created by dimitri.gamkrelidze on 4/15/2016.
 */
public class CSVTask extends Task implements CSVEventListener {


    private String source;
    private boolean pdf = false;
    private String mapping;
    private CustomerRowConverter converter;

    private Customer customer;

    public CSVTask(String source, String mapping, CustomerRowConverter converter, Customer customer) {
        this.source = source;
        this.converter = converter;
        this.mapping = mapping;
        this.customer = customer;


        detectIfPDF();
    }

    private void detectIfPDF() {
        if (Util.isEmpty(mapping))
            return;
        try (FileInputStream is = new FileInputStream(mapping)) {
            pdf = Util.detectIfPDF(is);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Object call() throws Exception {
        createStreams(null);
        return null;
    }

    public Map<Integer, Map<Integer, Map<String, String>>> createStreams(Integer maxRecords) throws Exception {
        try (InputStreamReader reader = new InputStreamReader(new FileInputStream(source));
             InputStream mappingReader = Util.notEmpty(mapping) ? new FileInputStream((mapping)) : null;
        ) {
            return new CSVCreator().addEventListeners(this).parse(customer, reader, mappingReader, converter, pdf, maxRecords);
        } catch (Throwable e) {
            UI.error("Error", "Error", "", e);
            throw e;

        }
    }

    @Override
    public void proceed(int size, int currentPos) {
        updateProgress(currentPos, size);
    }
}

