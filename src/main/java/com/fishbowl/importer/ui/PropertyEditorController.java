package com.fishbowl.importer.ui;

import com.fishbowl.importer.configuration.HttpParam;
import com.fishbowl.importer.configuration.Pair;
import com.fishbowl.importer.ui.classes.PropertyField;
import com.fishbowl.importer.ui.utils.TextAreaTableCell;
import com.fishbowl.importer.utils.Util;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.apache.http.entity.ContentType;

import java.net.URL;
import java.util.*;

/**
 * Created by dimitri.gamkrelidze on 4/18/2016.
 */
public class PropertyEditorController extends Application implements Initializable {

    @FXML
    private TableView<PropertyField> tblProperty;

    @FXML
    private TableColumn<PropertyField, String> tcKey;

    @FXML
    private TableColumn<PropertyField, String> tcValue;


    @FXML
    private TableColumn<PropertyField, Boolean> tcRBody;

    @FXML
    private TableColumn<PropertyField, Boolean> tcRPart;

    @FXML
    private ContextMenu cmMain;

    private ObservableList<PropertyField> allData = FXCollections.observableArrayList();

    public static void main(String[] args) throws Exception {
        launch(args);

    }

    public void setType(boolean simple) {
        tcRBody.setVisible(!simple);
        tcRPart.setVisible(!simple);

    }

    public void clearData() {
        allData.clear();
    }

    public void setData(Map<String, String> data) {
        if (data != null && !data.isEmpty())
            for (String key : data.keySet()) {
                String value = data.get(key);
                allData.add(new PropertyField(key, value));
            }
    }

    public List<HttpParam> getData() {
        List<HttpParam> result = new ArrayList<>();
        for (PropertyField field : allData) {
            result.add(new HttpParam(field.getTcKey(), field.getTcValue(), field.getTcRequestBody(), field.getTcRequestPart()));
        }
        return result;
    }

    public void setData(List<HttpParam> data) {
        if (data != null && !data.isEmpty())
            for (HttpParam key : data) {
                allData.add(new PropertyField(key.getParamName(), key.getParamValue(), key.getRequestBody(), key.getRequestBody()));
            }
    }

    public Map<String, String> getDataMap() {
        Map<String, String> result = new HashMap<>();
        for (PropertyField field : allData) {
            result.put(field.getTcKey(), field.getTcValue());
        }
        return result;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        tcKey.setCellFactory(TextFieldTableCell.forTableColumn());
        tcValue.setCellFactory(TextAreaTableCell.forTableColumn(new StringConverter<String>() {
            @Override
            public String toString(String object) {
                return object == null ? null : object.trim();
            }

            @Override
            public String fromString(String string) {
                return string == null ? null : string.trim();
            }
        }));

        tcRBody.setCellFactory(CheckBoxTableCell.forTableColumn(tcRBody));
        tcRPart.setCellFactory(CheckBoxTableCell.forTableColumn(tcRPart));


        tcKey.setCellValueFactory(
                new PropertyValueFactory<>("tcKey"));
        tcValue.setCellValueFactory(
                new PropertyValueFactory<>("tcValue"));

        tcRBody.setCellValueFactory(
                new PropertyValueFactory<>("tcRequestBody"));
        tcRPart.setCellValueFactory(
                new PropertyValueFactory<>("tcRequestPart"));

//        tblProperty.setEditable(true);
        tblProperty.setItems(allData);
        tblProperty.setContextMenu(null);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pair<Region, PropertyEditorController> loader = UI.loadWithController("forms/property_editor.fxml", PropertyEditorController.class);

        primaryStage.setScene(new Scene(loader.getLeft()));

        Util.generateStaticValues(ContentType.class);
        Map<String, List<String>> types = new HashMap<>();
        types.put("Content-Type", Util.generateStaticValues(ContentType.class));
        Map<String, String> data = new HashMap<>();
        data.put("bb", "cc");
        primaryStage.setMaximized(true);
        primaryStage.show();
        UI.primaryStage = primaryStage;
    }


    @FXML
    void deleteAction(ActionEvent event) {
        PropertyField value = tblProperty.getSelectionModel().getSelectedItem();
        if (value == null)
            return;
        int index = tblProperty.getSelectionModel().getSelectedIndex();
        if (UI.ask("Alert", "Do you want to delete item?", ""))
            tblProperty.getItems().remove(index);
    }


    @FXML
    void newAction(ActionEvent event) {
        addData(new PropertyField(null, null));
    }


    private void addData(PropertyField field) {
        allData.add(field);
        tblProperty.getSelectionModel().select(allData.size() - 1);
    }


    public void enableDisableEditing(boolean enable) {
        tblProperty.setEditable(enable);
        tblProperty.setContextMenu(enable ? cmMain : null);
    }
}
