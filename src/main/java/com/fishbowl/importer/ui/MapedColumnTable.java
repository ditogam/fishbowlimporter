package com.fishbowl.importer.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fishbowl.importer.configuration.MappedColumn;
import com.fishbowl.importer.ui.classes.MappedFieldProperty;
import com.fishbowl.importer.ui.classes.Validator;
import com.fishbowl.importer.utils.Util;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by dito on 4/16/16.
 */
public class MapedColumnTable implements Initializable {


    @FXML
    TableView<MappedFieldProperty> tblMapedColumns;


    @FXML
    TableColumn<MappedFieldProperty, String> tcSource;
    @FXML
    TableColumn<MappedFieldProperty, String> tcDest;

    @FXML
    TableColumn<MappedFieldProperty, String> tcType;

    @FXML
    TableColumn<MappedFieldProperty, String> tcValue;

    private Validator validator;
    private List<MappedColumn> list;

    private boolean simple = false;

    @FXML
    private MenuItem editItem;

    @FXML
    private ContextMenu cmMain;

    public static MapedColumnTable createTable(Validator validator, ChangeListener<MappedFieldProperty> changeListener, boolean hide_dest) throws Exception {

        FXMLLoader loader = UI.getLoader("forms/maped_column_table.fxml");
        Scene scene = new Scene(loader.load());
        MapedColumnTable controller = loader.getController();
        if (changeListener != null)
            controller.tblMapedColumns.getSelectionModel().selectedItemProperty().addListener(changeListener);
        if (hide_dest)
            controller.tcDest.setVisible(false);
        controller.validator = validator;
        controller.tblMapedColumns.setContextMenu(controller.cmMain);
        if (hide_dest)
            for (TableColumn<MappedFieldProperty, ?> column : controller.tblMapedColumns.getColumns()) {
                column.setSortable(false);
            }
        if (!hide_dest)
            controller.cmMain.getItems().remove(controller.editItem);

        controller.simple = hide_dest;
        System.out.println();
        return controller;
    }

    public static MapedColumnTable createTable(boolean hide_dest) throws Exception {
        return createTable(null, null, hide_dest);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tcSource.setCellValueFactory(
                new PropertyValueFactory<MappedFieldProperty, String>("tcSource"));
        tcDest.setCellValueFactory(
                new PropertyValueFactory<MappedFieldProperty, String>("tcDest"));


        tcType.setCellValueFactory(
                new PropertyValueFactory<MappedFieldProperty, String>("tcType"));
        tcValue.setCellValueFactory(
                new PropertyValueFactory<MappedFieldProperty, String>("tcValue"));
        tblMapedColumns.setContextMenu(null);
    }

    private void add(MappedFieldProperty p) {
        if (!simple) {
            tblMapedColumns.getItems().add(p);
            tblMapedColumns.getSelectionModel().select(p);
            tblMapedColumns.scrollTo(tblMapedColumns.getItems().size() - 1);
        } else {
            try {
                showDialog(p, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showDialog(final MappedFieldProperty p, final boolean newItem) throws Exception {

        FXMLLoader loader = UI.getLoader("forms/mapfield_editor.fxml");
        BorderPane root = loader.load();
        final MappedFieldController fieldController = loader.getController();
        fieldController.setMappedColumn(p);
        fieldController.setSimple();

        Dialog<MappedFieldProperty> dialog = new Dialog<>();
        dialog.setTitle("Add/Edit");

        dialog.setResizable(true);


        dialog.getDialogPane().setContent(root);

        ButtonType buttonTypeOk = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
        final Button okButton = (Button) dialog.getDialogPane().lookupButton(buttonTypeOk);


        okButton.addEventFilter(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    p.setNewValue(fieldController.validate());
                } catch (Exception e) {
                    event.consume(); //not valid
                    UI.error("Error", e.getMessage(), "", e);
                    e.printStackTrace();
                }
            }
        });


        dialog.setResultConverter(new Callback<ButtonType, MappedFieldProperty>() {
            @Override
            public MappedFieldProperty call(ButtonType b) {

                if (b == buttonTypeOk) {
                    return p;

                }

                return null;
            }
        });

        Optional<MappedFieldProperty> result = dialog.showAndWait();

        if (result.isPresent()) {

            if (newItem)
                tblMapedColumns.getItems().add(p);

            tblMapedColumns.refresh();
        }
    }

    @FXML
    public void newAction(ActionEvent event) {
        if (validator != null) {
            validator.validate();
        }
        add(new MappedFieldProperty(new MappedColumn()));

    }

    @FXML
    public void editAction(ActionEvent event) {
        if (validator != null) {
            validator.validate();
        }
        MappedFieldProperty mappedFieldProperty = tblMapedColumns.getSelectionModel().getSelectedItem();
        if (mappedFieldProperty == null)
            return;
        try {
            showDialog(mappedFieldProperty, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @FXML
    public void dublicateAction(ActionEvent event) {
        if (validator != null) {
            validator.validate();
        }
        MappedFieldProperty mappedFieldProperty = tblMapedColumns.getSelectionModel().getSelectedItem();
        if (mappedFieldProperty == null)
            return;
        ObjectMapper om = new ObjectMapper();
        try {
            String str = om.writeValueAsString(mappedFieldProperty.getMappedColumn());
            MappedColumn m = om.readValue(str, MappedColumn.class);
            add(new MappedFieldProperty(m));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void deleteAction(ActionEvent event) {
        MappedFieldProperty mappedFieldProperty = tblMapedColumns.getSelectionModel().getSelectedItem();
        if (mappedFieldProperty == null)
            return;
        int index = tblMapedColumns.getSelectionModel().getSelectedIndex();
        if (UI.ask("Alert", "Do you want to delete item?", ""))
            tblMapedColumns.getItems().remove(index);
    }

    public TableView<MappedFieldProperty> getTblMapedColumns() {
        return tblMapedColumns;
    }

    public boolean isEmpty() {
        return tblMapedColumns.getItems().size() < 2;
    }

    public List<MappedColumn> validate() throws Exception {
        if (isEmpty()) {
            throw new Exception("Please add at least 2 concatenated fields");
        }
        return getItems();
    }


    public void clear() {
        tblMapedColumns.getItems().clear();
    }

    public List<MappedColumn> getItems() {
        List<MappedColumn> result = new ArrayList<>();
        for (MappedFieldProperty property : tblMapedColumns.getItems()) {
            result.add(property.getMappedColumn());
        }
        return result;
    }

    public void setItems(List<MappedColumn> list) {
        this.list = list;
        if (list == null) {
            clear();
            return;
        }
        List<MappedFieldProperty> fieldProperties = new ArrayList<>();
        for (MappedColumn mappedColumn : list) {
            fieldProperties.add(new MappedFieldProperty(mappedColumn));
        }

        tblMapedColumns.getItems().addAll(fieldProperties);
        if (Util.notEmpty(fieldProperties))
            tblMapedColumns.getSelectionModel().select(0);
    }

    public void enableDisableEditing(boolean enable) {
        tblMapedColumns.setContextMenu(enable ? cmMain : null);
    }


}
