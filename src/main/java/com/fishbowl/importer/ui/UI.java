package com.fishbowl.importer.ui;

import com.fishbowl.importer.configuration.Pair;
import com.fishbowl.importer.service.CustomerService;
import com.sun.javafx.scene.control.skin.ComboBoxListViewSkin;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.Optional;

/**
 * Created by dimitri.gamkrelidze on 4/14/2016.
 */
public class UI extends Application {


    public static Stage primaryStage;
    public static Scene primaryScene;
    public static Scene myScene;
    private static CustomerService customerService;

    public static void inform(String title, String header, String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        setAlertValues(title, header, text, alert);
        alert.showAndWait();
    }

    protected static void setAlertValues(String title, String header, String text, Alert alert) {
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(text);
    }

    public static void warning(String title, String header, String text) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        setAlertValues(title, header, text, alert);
        alert.showAndWait();
    }

    public static void error(String title, String header, String question, Throwable th) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        setAlertValues(title, header, question, alert);

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        th.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    public static boolean ask(String title, String header, String question) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        setAlertValues(title, header, question, alert);
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/spring/root-context.xml");
        startWithCustomerService(applicationContext.getBean(CustomerService.class));
    }


    public static void startWithCustomerService(CustomerService customerService) {
        UI.customerService = customerService;
        launch(null);
    }


    public static Parent load(String resource) throws Exception {
        URL url = ClassLoader.getSystemClassLoader().getResource(resource);
        Parent root = FXMLLoader.load(url);
        return root;
    }

    public static FXMLLoader getLoader(String resource) throws Exception {
        URL url = ClassLoader.getSystemClassLoader().getResource(resource);
        FXMLLoader loader = new FXMLLoader(url);
        return loader;
    }

    public static <T extends Initializable> Pair<Region, T> loadWithController(String resource, Class<T> clazz) throws Exception {
        FXMLLoader loader = getLoader(resource);
        Region root = loader.load();
        T controller = loader.getController();
        Pair<Region, T> pair = new Pair<>(root, controller);
        return pair;
    }


    public static void anchor0(Node node) {
        AnchorPane.setTopAnchor(node, 0.0);
        AnchorPane.setRightAnchor(node, 0.0);
        AnchorPane.setLeftAnchor(node, 0.0);
        AnchorPane.setBottomAnchor(node, 0.0);
    }

    public static <T> void setComboValue(ComboBox<T> cb, T value) {
        if (value == null) {
            cb.setValue(null);
        }
        cb.setValue(value);
        value = cb.getValue();
        if (value == null)
            return;
        int index=cb.getSelectionModel().getSelectedIndex();
        if(index<0)
            return;
        ComboBoxListViewSkin<T> skin = (ComboBoxListViewSkin<T>) cb.getSkin();
        ((ListView<?>) skin.getPopupContent()).scrollTo(index);
    }


    public static void fit(Region pane, Pane parent) {
        anchor0(pane);

        pane.maxWidthProperty().bind(parent.widthProperty());
        pane.minWidthProperty().bind(parent.widthProperty());
        pane.maxHeightProperty().bind(parent.heightProperty());
        pane.minHeightProperty().bind(parent.heightProperty());
    }

    public static Pair<Scene, CustomerDesignController> crateateEditor(Stage primaryStage, final CustomerService customerService) throws Exception {
        FXMLLoader loader = getLoader("forms/customer_editor.fxml");
        Scene scene = new Scene(loader.load());
        primaryScene = scene;
        final CustomerDesignController designController = loader.getController();

        primaryStage.setTitle("fishbowl Loader");
        primaryStage.setScene(scene);

        primaryStage.setMaximized(true);

        primaryStage.addEventHandler(WindowEvent.WINDOW_SHOWN, new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent window) {

                designController.setCustomerService(customerService);
            }
        });

        UI.primaryStage = primaryStage;
        return new Pair<>(scene, designController);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        crateateEditor(primaryStage, customerService);
        primaryStage.show();


    }


}
