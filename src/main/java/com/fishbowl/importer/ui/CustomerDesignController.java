package com.fishbowl.importer.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fishbowl.importer.configuration.*;
import com.fishbowl.importer.service.CustomerService;
import com.fishbowl.importer.ui.classes.CustomerWrapper;
import com.fishbowl.importer.ui.classes.MappedFieldProperty;
import com.fishbowl.importer.ui.classes.Validator;
import com.fishbowl.importer.ui.utils.ClassValidation;
import com.fishbowl.importer.ui.utils.FieldValidatorException;
import com.fishbowl.importer.utils.ColumnConverter;
import com.fishbowl.importer.utils.Util;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import org.apache.commons.beanutils.PropertyUtils;
import org.controlsfx.control.CheckComboBox;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;

/**
 * Created by dimitri.gamkrelidze on 4/14/2016.
 */
public class CustomerDesignController implements Initializable {
    @FXML
    AnchorPane pnlMappedFields;
    @FXML
    BorderPane mainContainer;
    @FXML
    ListView<CustomerWrapper> lvCustomers;
    @FXML
    AnchorPane tablePlace;
    @FXML
    TextField tiUserName;
    @FXML
    TextField tiPassword;


    MappedFieldController fieldController;

    boolean setting = false;
    @FXML
    private StackPane apLogin;
    @FXML
    private StackPane apDownload;
    @FXML
    private GridPane grContainer;


    @FXML
    private BorderPane pCustomerPane;
    @FXML
    private GridPane pDownloadPane;


    @FXML
    private TextField tfUser;

    @FXML
    private PasswordField pfPassword;


    @FXML
    private TextField vdefaultSourceDateFormat;

    @FXML
    private TextField vdefaultDestDateFormat;


    @FXML
    private Button btnNew;

    @FXML
    private Button btnDublicate;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnEdit;


    @FXML
    private ComboBox<Pair<Long, String>> cbFBCustomer;

    @FXML
    private ComboBox<String> cbUniqueColumn;

    @FXML
    private AnchorPane anSameItems;

    private CheckComboBox<String> cbSameItems;


    private CustomerService customerService;


    private Customers customers;
    private ObservableList<CustomerWrapper> items;


    private MapedColumnTable mapedColumnTable;
    private HttpController loginController;
    private HttpController downloadController;
    private Customer customer;
    private MappedFieldProperty currentValue = null;
    private List<String> fieldNames = new ArrayList<>();
    private List<Node> editorControls = new ArrayList<>();
    private List<Node> newControls = new ArrayList<>();
    private boolean changed = false;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        editorControls.addAll(Arrays.asList(pCustomerPane, pDownloadPane, pnlMappedFields, btnSave, btnCancel));
        newControls.addAll(Arrays.asList(btnDublicate, btnNew, btnEdit));


        loginController = createHttpController(apLogin);
        downloadController = createHttpController(apDownload);
        try {
            for (Field field : Customer.class.getDeclaredFields()) {
                fieldNames.add(field.getName());
            }
            FXMLLoader loader = UI.getLoader("forms/mapfield_editor.fxml");
            lvCustomers.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                try {
                    setCustomer(newValue.getCustomer());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            Validator validator = new Validator() {
                @Override
                public void validate() {
                    if (currentValue != null)
                        CustomerDesignController.this.validateValue(currentValue);
                }
            };
            mapedColumnTable = MapedColumnTable.createTable(validator, (observable, oldValue, newValue) -> {
                if (!setting && oldValue != null)
                    validateValue(oldValue);
                fieldController.setMappedColumn(newValue);

                currentValue = newValue;
            }, false);


            BorderPane root = loader.load();
            fieldController = loader.getController();
            UI.anchor0(root);
            pnlMappedFields.getChildren().addAll(root);
            UI.anchor0(mapedColumnTable.getTblMapedColumns());
            tablePlace.getChildren().addAll(mapedColumnTable.getTblMapedColumns());
            cbSameItems = new CheckComboBox<>(FXCollections.observableArrayList(ColumnConverter.getAZ(false)));
            anSameItems.getChildren().addAll(cbSameItems);
            UI.anchor0(anSameItems);
            cbUniqueColumn.getItems().addAll(ColumnConverter.getAZ(true));

        } catch (Exception e) {
            e.printStackTrace();
        }
        enableDisableEditing(false);
    }

    private void enableDisableEditing(boolean enable) {

        try {
            for (Node control : editorControls) {
                control.setDisable(!enable);
            }
            for (Node control : newControls) {
                control.setDisable(enable);
            }
            if (items.isEmpty())
                btnEdit.setDisable(true);
            lvCustomers.setDisable(enable);
            mapedColumnTable.enableDisableEditing(enable);
            downloadController.enableDisableEditing(enable);
            loginController.enableDisableEditing(enable);
        } catch (Exception e) {
        }

    }

    public void validateValue(MappedFieldProperty oldValue) {
        try {
            oldValue.setNewValue(fieldController.validate());

        } catch (Exception ex) {
            UI.error("Error", ex.getMessage(), "Changes was not saved", ex);

        }
    }

    private void setFieldValue(Node node, Object value) {
        if (node == null)
            return;

        if (node instanceof TextField)
            ((TextField) node).setText(value == null ? null : value.toString());
        if (node instanceof CheckBox)
            ((CheckBox) node).setSelected((value == null ? false : (value instanceof Boolean ? ((Boolean) value).booleanValue() : false)));
    }

    private void clearFields() {

        loginController.clearData();
        downloadController.clearData();
        fieldController.clearFields();
        mapedColumnTable.clear();
        tfUser.setText(null);
        pfPassword.setText(null);

        tiUserName.setText(null);
        tiPassword.setText(null);
        cbFBCustomer.setValue(null);
        cbUniqueColumn.setValue(null);
        cbSameItems.getCheckModel().clearChecks();

        Scene scene = getScene();
        for (String fieldName : fieldNames) {
            Node node = scene.lookup("#v" + fieldName);
            setFieldValue(node, null);
        }
    }

    private Scene getScene() {
        Scene scene = grContainer.getScene();
        return scene == null ? UI.primaryScene : scene;
    }




    private void setCustomer(Customer newValue) throws Exception {
        setting = true;
        try {
            clearFields();
            if (newValue == null)
                return;
            this.customer = newValue;
            fieldController.setMappedColumn(null);
            newValue.parse();
            if (newValue.getMappedId() != null) {
                UI.setComboValue(cbFBCustomer,new Pair<>(newValue.getMappedId(), ""));
            }
            if (newValue.getDuplicateColumn() != null && Util.notEmpty(newValue.getDuplicateColumn().getSourceA())) {
                UI.setComboValue(cbUniqueColumn,newValue.getDuplicateColumn().getSourceA());
            }

            if (Util.notEmpty(newValue.getSameColumns())) {
                for (MappedColumn column : newValue.getSameColumns()) {
                    if (Util.notEmpty(column.getSourceA()))
                        cbSameItems.getCheckModel().check(column.getSourceA());
                }
            }

            Scene scene = getScene();
            if (newValue.getFileDownloadOptions() != null) {
                FileDownloadOptions dOpt = newValue.getFileDownloadOptions();
                loginController.setData(dOpt.getLoginOptions());
                downloadController.setData(dOpt.getDownloadOptions());
                tiUserName.setText(dOpt.getUserNameParamName());
                tiPassword.setText(dOpt.getUserPasswordParamName());
                tfUser.setText(dOpt.getUserName());
                pfPassword.setText(dOpt.getUserPassword());
            }
            vdefaultSourceDateFormat.setText(newValue.getDefaultSourceDateFormat());
            vdefaultDestDateFormat.setText(newValue.getDefaultDestDateFormat());

            mapedColumnTable.setItems(newValue.getMappedColumns());

            Map<String, Object> values = PropertyUtils.describe(newValue);
            for (String key : values.keySet()) {
                Node node = scene.lookup("#v" + key);
                setFieldValue(node, values.get(key));
            }
        } finally {
            setting = false;
        }
    }

    public void setCustomers(Customers customers) {

        this.customers = customers;
        if (customers != null) {
            items = FXCollections.observableArrayList(CustomerWrapper.wrap(customers.getCustomers()));
            lvCustomers.setItems(items);
            if (Util.notEmpty(customers.getCustomers()))
                lvCustomers.getSelectionModel().select(0);
        }
    }

    private HttpController createHttpController(Pane pane) {
        try {
            Pair<Region, HttpController> loader = UI.loadWithController("forms/http_editor.fxml", HttpController.class);
            UI.fit(loader.getLeft(), pane);
            pane.getChildren().addAll(loader.getLeft());
            return loader.getRight();

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    @FXML
    void saveValue(ActionEvent event) {
        Customer c = new Customer();
        if (customer != null) {
            c.setId(customer.getId());
        }
        try {
            ClassValidation.validate(c, getScene());
            c.setMappedColumns(mapedColumnTable.getItems());
            c.setFileDownloadOptions(new FileDownloadOptions());
            c.getFileDownloadOptions().setLoginOptions(loginController.getData());
            c.getFileDownloadOptions().setDownloadOptions(downloadController.getData());
            c.getFileDownloadOptions().setUserNameParamName(Util.getStrValue(tiUserName.getText()))
                    .setUserPasswordParamName(Util.getStrValue(tiPassword.getText()))
                    .setUserName(Util.getStrValue(tfUser.getText()))
                    .setUserPassword(Util.getStrValue(pfPassword.getText()));

            Pair<Long, String> cv = cbFBCustomer.getValue();
            if (cv != null)
                c.setMappedId(cv.getLeft());
            String unique = cbUniqueColumn.getValue();
            c.setDuplicateColumn(null);
            c.setSameColumns(null);

            if (Util.notEmpty(unique))
                c.setDuplicateColumn(MappedColumn.fromSource(unique));
            ObservableList<String> list = cbSameItems.getCheckModel().getCheckedItems();
            if (Util.notEmpty(list)) {
                c.setSameColumns(new ArrayList<>());
                for (String s : list) {
                    c.getSameColumns().add(MappedColumn.fromSource(s));
                }
            }
            ObjectMapper om = new ObjectMapper();
            try {

                String doOpt = om.writeValueAsString(c.getFileDownloadOptions().getLoginOptions());
                if (doOpt.equals("{}"))
                    c.getFileDownloadOptions().setLoginOptions(null);
                doOpt = om.writeValueAsString(c.getFileDownloadOptions().getDownloadOptions());
                if (doOpt.equals("{}"))
                    c.getFileDownloadOptions().setDownloadOptions(null);
                doOpt = om.writeValueAsString(c.getFileDownloadOptions());
                if (doOpt.equals("{}"))
                    c.setFileDownloadOptions(null);
                int index = items.size();
                if (c.getId() == null) {
                    customerService.save(c);
                    items.add(new CustomerWrapper(c));
                } else {
                    index = lvCustomers.getSelectionModel().getSelectedIndex();
                    customerService.save(c);
                    items.get(index).setCustomer(c);
                }

//                lvCustomers.setItems(null);
//                lvCustomers.setItems(items);
                lvCustomers.refresh();
                lvCustomers.getSelectionModel().select(index);
                lvCustomers.scrollTo(index);
                CustomerDesignController.this.changed = true;
                setCustomer(c);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (FieldValidatorException e) {
            UI.error("Error", e.getMessage(), "", e);
            if (e.getNode() != null)
                e.getNode().requestFocus();
        }
        enableDisableEditing(false);
    }


    private void createCustomer(Customer customer) {
        enableDisableEditing(true);
        try {
            setCustomer(customer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void dublicateAction(ActionEvent event) {
        Customer c = lvCustomers.getSelectionModel().getSelectedItem().getCustomer();
        ObjectMapper om = new ObjectMapper();
        try {
            String json = om.writeValueAsString(c);
            c = om.readValue(json, Customer.class);
            c.setId(null);
            c.clearMainFields();
            createCustomer(c);
        } catch (Exception e) {

        }


    }

    @FXML
    void newAction(ActionEvent event) {
        createCustomer(null);
    }

    @FXML
    void cancelAction(ActionEvent event) {
        try {
            setCustomer(lvCustomers.getSelectionModel().getSelectedItem().getCustomer());
        } catch (Exception e) {
            e.printStackTrace();
        }
        enableDisableEditing(false);
    }

    @FXML
    void editAction(ActionEvent event) {
        enableDisableEditing(true);
    }


    @FXML
    void createDBAction(ActionEvent event) {
        if (UI.ask("Alert", "Do you want to delete item?", "")) {
            try {
                customerService.recreateDB();
                setCustomerService(customerService);
                UI.inform("Info", "Suceed", "Database recreated successfully");
            } catch (Throwable ex) {
                ex.printStackTrace();
                UI.error("Error", ex.getMessage(), "Changes was not saved", ex);
            }
        }

    }


    public void setCustomerService(CustomerService customerService) {
        try {
            this.customerService = customerService;
            Customers c = new Customers();
            c.setCustomers(customerService.getAll());
            setCustomers(c);
            cbFBCustomer.getItems().addAll(customerService.getAllFishblowCustomers());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isChanged() {
        return changed;
    }
}
