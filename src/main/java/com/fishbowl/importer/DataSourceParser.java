package com.fishbowl.importer;

import com.fishbowl.importer.utils.CLI;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import javax.sql.DataSource;
import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by dimitri.gamkrelidze on 4/21/2016.
 */
public class DataSourceParser implements DataSource {

    private static File defaultFile = new File(new File(System.getProperty("user.dir")),"ds.xml");


    private DataSource real_ds = null;
    private String dialect;


    public DataSourceParser() throws Exception {
        ApplicationContext applicationContext = getApplicationContext();
        real_ds = applicationContext.getBean(DataSource.class);
        dialect = applicationContext.getBean("dialect", String.class);
        System.out.println("Creating successfully");
    }

    private ApplicationContext getApplicationContext() {
        ApplicationContext applicationContext = null;
        List<File> files = new ArrayList<>(Arrays.asList(defaultFile));

        if (CLI.property_file != null)
            files.add(0, CLI.property_file);
        for (File file : files) {
            try {
                applicationContext = createAppContext(file);
                break;
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }


        if (applicationContext == null)
            applicationContext = new ClassPathXmlApplicationContext("/spring/ds.xml");
        return applicationContext;
    }

    private ApplicationContext createAppContext(File property_file) throws Exception {
        String file=property_file.toURI().toString();
        return new FileSystemXmlApplicationContext(file);
    }


    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return real_ds.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        real_ds.setLogWriter(out);

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return real_ds.getLoginTimeout();
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        real_ds.setLoginTimeout(seconds);

    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return real_ds.getParentLogger();
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return real_ds.isWrapperFor(iface);
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return real_ds.unwrap(iface);
    }

    @Override
    public Connection getConnection() throws SQLException {
        return real_ds.getConnection();
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return real_ds.getConnection(username, password);
    }


    public String getDialect() {
        return dialect;
    }

    public void dispose() {
//        real_ds.
    }
}
