package com.fishbowl.importer.utils;


import org.apache.commons.cli.*;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * Created by dimitri.gamkrelidze on 4/21/2016.
 */
public class CLI {


    public static File property_file;

    public static void printUsage(
            final String applicationName,
            final Options options,
            final OutputStream out) {
        final PrintWriter writer = new PrintWriter(out);
        final HelpFormatter usageFormatter = new HelpFormatter();
        usageFormatter.printUsage(writer, 80, applicationName, options);
//        writer.close();
    }

    /**
     * Write "help" to the provided OutputStream.
     */
    public static void printHelp(
            final Options options,
            final int printedRowWidth,
            final String header,
            final String footer,
            final int spacesBeforeOption,
            final int spacesBeforeOptionDescription,
            final boolean displayUsage,
            final OutputStream out) {
        final String commandLineSyntax = "java -cp ApacheCommonsCLI.jar";
        final PrintWriter writer = new PrintWriter(out);
        final HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp(
                writer,
                printedRowWidth,
                commandLineSyntax,
                header,
                options,
                spacesBeforeOption,
                spacesBeforeOptionDescription,
                footer,
                displayUsage);
        writer.close();
    }

    public static void usePosixParser(final String[] commandLineArguments) {
        final CommandLineParser cmdLinePosixParser = new DefaultParser();
        final Options posixOptions = new Options();
        Option property_file = Option.builder().argName("p").longOpt("property.file").required(false).build();
        posixOptions.addOption("p", "property.file", true, "Property file name");
        for (Option option : posixOptions.getOptions()) {
            property_file = option;
        }
        String applicationName = "Application commandline usage";


        System.out.println("-- USAGE --");
        printUsage(applicationName + " (Posix)", posixOptions, System.out);


        CommandLine commandLine;
        try {
            commandLine = cmdLinePosixParser.parse(posixOptions, commandLineArguments);

            if (commandLine.hasOption(property_file.getOpt())) {
                CLI.property_file = new File(commandLine.getOptionValue(property_file.getOpt()));
                if (!CLI.property_file.exists() || !CLI.property_file.isFile())
                    CLI.property_file = null;
            }
        } catch (Exception parseException)  // checked exception
        {
            parseException.printStackTrace();
        }
    }
}
