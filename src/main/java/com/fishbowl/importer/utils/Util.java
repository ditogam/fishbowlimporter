package com.fishbowl.importer.utils;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dimitri.gamkrelidze on 4/13/2016.
 */
public class Util {
    private static Map<String, NumberFormat> numberFormatMap = new HashMap<>();
    private static Map<String, DateFormat> dateFormatMap = new HashMap<>();

    public static boolean has(Boolean val) {
        return val != null && val.booleanValue();
    }

    /**
     * Returns true if arr!=null and arr.length > 0
     *
     * @param arr
     * @return
     */
    public static boolean hasValue(Object[] arr) {
        return arr != null && arr.length > 0;
    }

    /**
     * Returns true when s is neither null nor an empty string
     *
     * @param s
     * @return
     */
    public static boolean hasValue(String s) {
        return s != null && !s.isEmpty();
    }

    public static String getStrValue(String str) {
        return Util.isEmpty(str) ? null : str.trim();
    }

    public static boolean isEmpty(String s) {
        return s == null || s.trim().isEmpty();
    }

    public static <T> boolean isEmpty(T[] s) {
        return s == null || s.length == 0;
    }

    public static <T, V> boolean isEmpty(Map<T, V> s) {
        return s == null || s.isEmpty();
    }

    public static <T> boolean isEmpty(List<T> s) {
        return s == null || s.isEmpty();
    }

    public static boolean isNullOrNegative(Number number) {
        return number == null || number.doubleValue() < 0;
    }

    public static boolean isNotNullOrNegative(Number number) {
        return !isNullOrNegative(number);
    }

    public static boolean notEmpty(String s) {
        return !isEmpty(s);
    }

    public static <T> boolean notEmpty(T[] s) {
        return !isEmpty(s);
    }

    public static <T> boolean notEmpty(List<T> s) {
        return !isEmpty(s);
    }

    public static boolean between(Number source, Number min, Number max) {
        return source.doubleValue() >= min.doubleValue() && source.doubleValue() <= max.doubleValue();
    }
    public static <T> List<List<T>> split(List<T> list, int size) throws NullPointerException, IllegalArgumentException {
        if (list == null) {
            throw new NullPointerException("The list parameter is null.");
        }
        if (size <= 0) {
            throw new IllegalArgumentException("The size parameter must be more than 0.");
        }

        int num = list.size() / size;
        int mod = list.size() % size;
        List<List<T>> ret = new ArrayList<List<T>>(mod > 0 ? num + 1 : num);
        for (int i = 0; i < num; i++) {
            ret.add(list.subList(i * size, (i + 1) * size));
        }
        if (mod > 0) {
            ret.add(list.subList(num * size, list.size()));
        }
        return ret;
    }
    public static NumberFormat getNumberFormat(String format) {
        return getNumberFormat(format, null);
    }

    public static NumberFormat getNumberFormat(String format, String default_val) {
        if (isEmpty(format))
            format = default_val;
        NumberFormat numberFormat = numberFormatMap.get(format);
        if (numberFormat == null) {
            numberFormat = new DecimalFormat(format);
            numberFormatMap.put(format, numberFormat);
        }
        return numberFormat;
    }


    public static DateFormat getDateFormat(String format) {
        return getDateFormat(format, null);
    }

    public static DateFormat getDateFormat(String format, String default_val) {
        if (isEmpty(format))
            format = default_val;
        DateFormat dateFormat = dateFormatMap.get(format);
        if (dateFormat == null) {
            dateFormat = new SimpleDateFormat(format);
            dateFormatMap.put(format, dateFormat);
        }
        return dateFormat;
    }

    public static final boolean detectIfPDF(InputStream is) throws Exception {
        byte[] data = new byte[5];
        is.read(data);

        if (data != null && data.length > 4 &&
                data[0] == 0x25 && // %
                data[1] == 0x50 && // P
                data[2] == 0x44 && // D
                data[3] == 0x46 && // F
                data[4] == 0x2D) {
            return true;
        }
        return false;
    }

    public static List<CSVRecord> getCsvRecordsRowNum(Integer maxRecords, CSVParser parser) {
        List<CSVRecord> records;
        records = new ArrayList<>();
        Iterator<CSVRecord> recordIterator = parser.iterator();
        for (Integer i = 0; i < maxRecords; i++) {
            if (!recordIterator.hasNext())
                break;
            records.add(recordIterator.next());
        }
        return records;
    }


    public static <E> List<String> generateEnumValues(Class<E> clazz) {
        List<String> result = new ArrayList<>();
        List<E> list = Arrays.asList(clazz.getEnumConstants());
        for (E e : list) {
            result.add(e.toString());
        }
        return result;
    }

    public static List<String> generateStaticValues(Class<?> clazz) {
        List<String> result = new ArrayList<>();
        for (Field field : clazz.getDeclaredFields()) {
            if (Modifier.isStatic(field.getModifiers()) && Modifier.isFinal(field.getModifiers()) && field.getName().equals(field.getName().toUpperCase())) {
                try {
                    String value = field.get(null).toString();
                    result.add(value);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }


}
