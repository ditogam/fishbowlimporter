package com.fishbowl.importer.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.dao.CustomerDao;
import com.fishbowl.importer.entity.ECustomer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean;

import javax.persistence.spi.PersistenceProvider;
import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * Created by dimitri.gamkrelidze on 4/15/2016.
 */
public class DBGenerator {


    public static void main(String[] args) throws Exception {
        System.out.println("Starting...");
        System.setProperty("hbm2ddl.auto", "create");
        System.setProperty("hibernate.show_sql", "true");
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring/root-context.xml");
        initData(context);
    }

    public static void initData(ApplicationContext context) throws Exception {
        generateSchema(context);

        ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.setContinueOnError(true);
        populator.setSeparator("/;");
        DataSource ds = context.getBean(DataSource.class);
        populator.addScript(new ClassPathResource("spring/db.sql"));
        populator.populate(ds.getConnection());

        CustomerDao customerDao = context.getBean(CustomerDao.class);
        ObjectMapper om = new ObjectMapper();
        om.enable(SerializationFeature.INDENT_OUTPUT);

        List<Customer> customers = Creator.createtCustomers().getCustomers();
        for (Customer customer : customers) {
            customer.parse();
            ECustomer eCustomer = new ECustomer();
            String json = om.writeValueAsString(customer);
            eCustomer.setJson(json);
            customerDao.save(eCustomer);

        }


        List<ECustomer> customerList = customerDao.getAll();
        for (ECustomer eCustomer : customerList) {
            System.out.println(eCustomer.getId());
        }
    }

    private static void generateSchema(ApplicationContext context) {
        AbstractEntityManagerFactoryBean factoryBean = context.getBean(AbstractEntityManagerFactoryBean.class);
        PersistenceProvider provider = factoryBean.getPersistenceProvider();
        Map<String, Object> map = factoryBean.getJpaPropertyMap();
        map.put("hibernate.hbm2ddl.auto", "create");
        map.put("hibernate.show_sql", "true");
        provider.generateSchema(factoryBean.getPersistenceUnitInfo(), map);
    }
}
