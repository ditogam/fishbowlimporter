package com.fishbowl.importer.utils;

/**
 * Created by dimitri.gamkrelidze on 4/13/2016.
 */
public class ColumnConverter {
    private final static String[] AZ = loadAZ();

    public static Integer convert(String value) {
        if (value == null)
            return null;
        int result = 0;
        for (int i = 0; i < value.length(); i++) {
            result *= 26;
            result += value.charAt(i) - 'A' + 1;
        }
        return result - 1;
    }

    public static String convert(Integer value) {
        if (value == null)
            return null;
        value++;
        final StringBuilder sb = new StringBuilder();

        int num = value - 1;
        while (num >= 0) {
            int numChar = (num % 26) + 65;
            sb.append((char) numChar);
            num = (num / 26) - 1;
        }
        return sb.reverse().toString();
    }


    private static String[] loadAZ() {
        String[] result = new String[ColumnConverter.convert("ZZ") + 1];
        for (int i = 0; i < result.length; i++) {
            result[i] = convert(i);
        }
        return result;
    }

    public static String[] getAZ(boolean addEmpty) {
        String[] result = new String[AZ.length + (addEmpty ? 1 : 0)];
        System.arraycopy(AZ, 0, result, (addEmpty ? 1 : 0), AZ.length);
        if (addEmpty)
            result[0] = "";
        return result;
    }

    public static String[] getAZ() {
        return getAZ(false);
    }

}
