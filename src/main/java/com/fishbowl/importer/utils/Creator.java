package com.fishbowl.importer.utils;


import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.configuration.Customers;
import com.fishbowl.importer.configuration.FileDownloadOptions;
import com.fishbowl.importer.configuration.MappedColumn;
import org.springframework.http.HttpMethod;

/**
 * Created by dimitri.gamkrelidze on 4/13/2016.
 */
public class Creator {

    public static Customers createtCustomers() {
        Customer c = new Customer().setId(1L).setCustomerName("MOBSTUB").
                setCustomerAddress("806 EASTERN PKWY").
                setCustomerCity("BROOKLYN").setCustomerState("NY").
                setCustomerZipCode("11213").
                setCustomerCountry("United States").setMappedId(10093L);
        c.setFileDownloadOptions(new FileDownloadOptions());
        c.getFileDownloadOptions().setLoginUrl("http://vendors.mobstub.com/auth/login")
                .setDownloadUrl("http://vendors.mobstub.com/deals/download_unshipped").
                setUserNameParamName("username").setUserPasswordParamName("password")
                .setUserName("erg").setUserPassword("Mobstub").addDownloadParams("action", "all")
                .addLoginParams("username", "username", true, true)
                .addLoginParams("password", "password", true, true)
                .setLoginMethod(HttpMethod.POST);

        c.addMappedColumn(MappedColumn.setCouple("A", "U"));
        c.addMappedColumn(MappedColumn.setCouple("B", "W").setDate(true));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("C", "H"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("E", "C"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("F", "F").setNumber(true));

        c.addMappedColumn(MappedColumn.setCouple("G", "L"));

        /**
         * This means that you need to write concatenated values in dest column, with concatanator
         */

        c.addMappedColumn(MappedColumn.forDest("M").
                setConcatenator(" ")
                .addConcatenatedColumn(MappedColumn.fromSource("H"))
                .addConcatenatedColumn(MappedColumn.fromSource("I"))
        );

        c.addMappedColumn(MappedColumn.setCouple("J", "N"));


        c.addMappedColumn(MappedColumn.setCouple("K", "O"));
        c.addMappedColumn(MappedColumn.setCouple("L", "P"));

        c.addMappedColumn(MappedColumn.setDestStaticValue("A", "SO"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("C", "20"));

        c.addMappedColumn(MappedColumn.setDestFromCustomerField("D", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("E", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("F", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("G", "customerAddress"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("H", "customerCity"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("I", "customerState"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("J", "customerZipCode"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("K", "customerCountry"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("Q", "customerCountry"));

        c.addMappedColumn(MappedColumn.setDestStaticValue("T", "30"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("S", "None"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("R", "Fedex"));

        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("A", "Item"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("B", "10"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("D", "1"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("E", "Ea"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("G", "False"));

        c.parse();

        Customers cst = new Customers();

//        new CSVCreator().parse(c, new InputStreamReader(ClassLoader.getSystemResourceAsStream("Mobsutb.csv")), new PrintWriter(System.out));


        cst.getCustomers().add(c);


        c = new Customer().setId(1L).setCustomerName("QUIBIDS HOLDINGS LLC").
                setCustomerAddress("4 NE 10TH STREET SUITE 242").
                setCustomerCity("OKLAHOMA CITY").setCustomerState("OK").
                setCustomerZipCode("73104").
                setCustomerCountry("United States").setMappedId(167L);

        cst.getCustomers().add(c);

        c.addMappedColumn(MappedColumn.setCouple("A", "U"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("B", "H"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("C", "C"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("D", "F").setNumber(true));
        c.addMappedColumn(MappedColumn.setCouple("E", "L"));


/**
 * This means that you need to write concatenated values in dest column, with concatanator
 */

        c.addMappedColumn(MappedColumn.forDest("M").
                setConcatenator(" ")
                .addConcatenatedColumn(MappedColumn.fromSource("E")).addConcatenatedColumn(MappedColumn.fromSource("F"))
        );


        c.addMappedColumn(MappedColumn.setCouple("H", "N"));


        c.addMappedColumn(MappedColumn.setCouple("I", "O"));
        c.addMappedColumn(MappedColumn.setCouple("J", "P"));
        c.addMappedColumn(MappedColumn.setCouple("M", "AG"));


        c.addMappedColumn(MappedColumn.setDestStaticValue("A", "SO"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("C", "20"));

        c.addMappedColumn(MappedColumn.setDestFromCustomerField("D", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("E", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("F", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("G", "customerAddress"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("H", "customerCity"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("I", "customerState"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("J", "customerZipCode"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("K", "customerCountry"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("Q", "customerCountry"));

        c.addMappedColumn(MappedColumn.setDestStaticValue("T", "30"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("S", "None"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("R", "UPS"));

        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("A", "Item"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("B", "10"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("D", "1"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("E", "ea"));

        c.parse();


        c = new Customer().setId(1L).setCustomerName("WOOT SERVICES LLC").
                setCustomerAddress("4121 INTERNATIONAL PARKWAY,  STE 900").
                setCustomerCity("CARROLLTON").setCustomerState("TX").
                setCustomerZipCode("75007").
                setCustomerCountry("USA").setMappedId(10508L);

        cst.getCustomers().add(c);


        c.addMappedColumn(MappedColumn.setCouple("S", "W").setDate(true));
        MappedColumn column = MappedColumn.setCoupleSecondRow("V", "F").setNumber(true);
        c.addMappedColumn(column);


        c.addMappedColumn(MappedColumn.setCouple("C", "U"));
        c.addMappedColumn(MappedColumn.setCouple("D", "L"));

        /**
         * This means that you need to write concatenated values in dest column, with concatanator
         */


        c.addMappedColumn(MappedColumn.forDest("M").
                setConcatenator(" ")
                .addConcatenatedColumn(MappedColumn.fromSource("E"))
                .addConcatenatedColumn(MappedColumn.fromSource("F"))
        );

        c.addMappedColumn(MappedColumn.setCouple("G", "N"));
        c.addMappedColumn(MappedColumn.setCouple("H", "O"));
        c.addMappedColumn(MappedColumn.setCouple("I", "P"));
        c.addMappedColumn(MappedColumn.setCouple("J", "Q"));

        c.addMappedColumn(MappedColumn.setCoupleSecondRow("M", "C"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("O", "D"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("P", "H"));


        c.addMappedColumn(MappedColumn.setCouple("U", "V"));

        c.addMappedColumn(MappedColumn.setCoupleSecondRow("V", "D"));


        c.addMappedColumn(MappedColumn.setDestStaticValue("A", "SO"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("B", "Blank"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("C", "20"));


        c.addMappedColumn(MappedColumn.setDestFromCustomerField("D", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("E", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("F", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("G", "customerAddress"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("H", "customerCity"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("I", "customerState"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("J", "customerZipCode"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("K", "customerCountry"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("Q", "customerCountry"));

        c.addMappedColumn(MappedColumn.setDestStaticValue("R", "UPS"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("S", "None"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("T", "10"));

        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("A", "Item"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("B", "10"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("E", "Ea"));

        c.parse();


        c = new Customer().setId(1L).setCustomerName("TANGA.COM").
                setCustomerAddress("25 S Arizona Pl. Suite 410").
                setCustomerCity("Chandler").setCustomerState("AZ").
                setCustomerZipCode("85225").
                setCustomerCountry("United States").setMappedId(169L);

        cst.getCustomers().add(c);


        c.addMappedColumn(MappedColumn.setCouple("O", "W").setDate(true));
        column = MappedColumn.setCoupleSecondRow("L", "F").setSourcePDF("B").setDestPDF("D").setNumber(true);
        c.addMappedColumn(column);


        c.addMappedColumn(MappedColumn.setCouple("A", "V"));
        c.addMappedColumn(MappedColumn.setCouple("B", "U"));

        /**
         * This means that you need to write concatenated values in dest column, with concatanator
         */

        c.addMappedColumn(MappedColumn.forDest("L")
                .addConcatenatedColumn(MappedColumn.fromSource("D"))
                .addConcatenatedColumn(MappedColumn.fromSource("E"))
        );

        c.addMappedColumn(MappedColumn.forDest("M")
                .addConcatenatedColumn(MappedColumn.fromSource("F"))
                .addConcatenatedColumn(MappedColumn.fromSource("G"))
        );


        c.addMappedColumn(MappedColumn.setCouple("H", "N"));
        c.addMappedColumn(MappedColumn.setCouple("I", "O"));
        c.addMappedColumn(MappedColumn.setCouple("J", "P"));
        c.addMappedColumn(MappedColumn.setCouple("K", "Q"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("L", "C"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("M", "D"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("N", "H"));


        c.addMappedColumn(MappedColumn.setDestStaticValue("A", "SO"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("C", "20"));

        c.addMappedColumn(MappedColumn.setDestFromCustomerField("D", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("E", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("F", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("G", "customerAddress"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("H", "customerCity"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("I", "customerState"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("J", "customerZipCode"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("K", "customerCountry"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("Q", "customerCountry"));

        c.addMappedColumn(MappedColumn.setDestStaticValue("R", "Fedex"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("S", "None"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("T", "40"));

        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("A", "Item"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("B", "10"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("E", "ea"));

        c.parse();


        c = new Customer().setId(1L).setCustomerName("ERG Distributors LLC").
                setCustomerAddress("3470 Rand Road, S. ").
                setCustomerCity("Plainfield").setCustomerState("NJ").
                setCustomerZipCode("07080").
                setCustomerCountry("United States").setMappedId(170L);

        cst.getCustomers().add(c);

        c.addMappedColumn(MappedColumn.setCouple("A", "U"));
        c.addMappedColumn(MappedColumn.setCouple("B", "L"));

        /**
         * This means that you need to write concatenated values in dest column, with concatanator
         */

        c.addMappedColumn(MappedColumn.forDest("M").
                setConcatenator(" ")
                .addConcatenatedColumn(MappedColumn.fromSource("C"))
                .addConcatenatedColumn(MappedColumn.fromSource("D"))
        );
        column = MappedColumn.setCoupleSecondRow("I", "F").setSourcePDF("A").setDestPDF("D").setNumber(true).setSourceNumberFormat("$###.##");
        c.addMappedColumn(column);
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("L", "H"));
        c.addMappedColumn(MappedColumn.setCoupleSecondRow("E", "C"));

        c.addMappedColumn(MappedColumn.setCouple("G", "L"));

        c.addMappedColumn(MappedColumn.setDestStaticValue("A", "SO"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("C", "20"));

        c.addMappedColumn(MappedColumn.setDestFromCustomerField("D", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("E", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("F", "customerName"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("G", "customerAddress"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("H", "customerCity"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("I", "customerState"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("J", "customerZipCode"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("K", "customerCountry"));
        c.addMappedColumn(MappedColumn.setDestFromCustomerField("Q", "customerCountry"));

        c.addMappedColumn(MappedColumn.setDestStaticValue("R", "30"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("S", "None"));
        c.addMappedColumn(MappedColumn.setDestStaticValue("T", "30"));

        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("A", "Item"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("B", "10"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("D", "1"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("E", "Ea"));
        c.addMappedColumn(MappedColumn.setDestStaticValueSecondRow("G", "False"));

        c.parse();


        return cst;
    }


}
