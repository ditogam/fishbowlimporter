package com.fishbowl.importer.utils;

/**
 * Created by dimitri.gamkrelidze on 4/21/2016.
 */

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Provider;
import java.security.Security;
import java.util.Base64;


public class AdvancedEncryptionStandard {
    public static final String PREFIX = "ENSC(";
    public static final String SUFFIX = ")";
    private String encryptionKey;

    public AdvancedEncryptionStandard(String encryptionKey) {
        this.encryptionKey = encryptionKey;
    }

    public AdvancedEncryptionStandard() {
        this("MZygpewJsCpRrfOr");
    }

    public static String encryptText(String text) throws Exception {
        return PREFIX + new AdvancedEncryptionStandard().encrypt(text) + SUFFIX;
    }

    public String encrypt(String plainText) throws Exception {
        Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);
        byte[] encryptedBytes = cipher.doFinal(plainText.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    public String decrypt(String encrypted) throws Exception {
        Cipher cipher = getCipher(Cipher.DECRYPT_MODE);
        byte[] plainBytes = cipher.doFinal(Base64.getDecoder().decode(encrypted));
        return new String(plainBytes);
    }

    private Cipher getCipher(int cipherMode)
            throws Exception {
        for (Provider provider : Security.getProviders()) {
            System.out.println(provider);
        }
        String encryptionAlgorithm = "AES";
        SecretKeySpec keySpecification = new SecretKeySpec(
                encryptionKey.getBytes("UTF-8"), encryptionAlgorithm);
        Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
        cipher.init(cipherMode, keySpecification);
        return cipher;
    }
}
