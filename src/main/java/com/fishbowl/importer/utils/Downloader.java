package com.fishbowl.importer.utils;

import com.fishbowl.importer.configuration.FileDownloadOptions;
import com.fishbowl.importer.configuration.HttpParam;
import com.fishbowl.importer.configuration.HttpParams;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by dimitri.gamkrelidze on 4/15/2016.
 */
public class Downloader {


    public static void main(String[] args) throws Exception {
        FileDownloadOptions options = new FileDownloadOptions().setLoginUrl("https://tbconline.ge/ibs/delegate/rest/auth/v1/login")
                .setDownloadUrl("https://tbconline.ge/ibs/delegate/rest/mymoney/cashflow/v1/outcomes/categories").
                        setUserNameParamName("username").setUserPasswordParamName("password")
                .setUserName("DITOGAM").setUserPassword("Ditogam1980").
                        addDownloadParams("currency", "GEL").
                        addDownloadParams("period", "LAST_12_MONTHS").addLoginParams("rememberUserName", "true", true, true).addLoginParams("language", "ka", true, true)
                .addLoginParams("username", "username", true, true).addLoginParams("password", "password", true, true);
        options.getLoginOptions().setMethod("POST");
        List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
        loggers.add(LogManager.getRootLogger());
        for (Logger logger : loggers) {
            logger.setLevel(Level.OFF);
        }
        Logger logger = LogManager.getLogger("org.apache.http");
        if (logger != null)
            logger.setLevel(Level.ALL);
        logger = LogManager.getLogger("org.springframework");
        if (logger != null)
            logger.setLevel(Level.ALL);
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();) {
            download(options, bos, "DITOGAM", "Ditogam1980");
            System.out.println(new String(bos.toByteArray()));
        }

    }

    public static void download(FileDownloadOptions fopt, OutputStream os, String user_name, String password) throws Exception {
        BasicCookieStore cookieStore = new BasicCookieStore();
        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .build();
        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpclient));
        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
        messageConverters.add(new MappingJackson2HttpMessageConverter());
        Map<String, String> additionalOptions = new HashMap<>();

        additionalOptions.put(fopt.getUserNameParamName(), user_name);
        additionalOptions.put(fopt.getUserPasswordParamName(), password);

        execute(null, fopt.getLoginOptions(), restTemplate, additionalOptions);
        execute(os, fopt.getDownloadOptions(), restTemplate, new HashMap<>());

    }


    private static void execute(OutputStream os, HttpParams httpParams, RestTemplate restTemplate, Map<String, String> additionalOptions) throws Exception {
        if (httpParams == null)
            return;
        if (Util.isEmpty(httpParams.getUrl()))
            return;
        if (additionalOptions == null)
            additionalOptions = new HashMap<>();
        if (httpParams.getParams() == null)
            httpParams.setParams(new ArrayList<>());
        List<HttpParam> params = combineOptions(httpParams.getParams(), additionalOptions);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", Util.isEmpty(httpParams.getContentType()) ? MediaType.APPLICATION_JSON_UTF8_VALUE : httpParams.getContentType());
        if (httpParams.getHeaders() != null)
            for (String s : httpParams.getHeaders().keySet()) {
                headers.set(s, httpParams.getHeaders().get(s));
            }
        Map<String, String> dataObjects = new HashMap<>();
        StringBuilder sb = new StringBuilder();

        for (HttpParam param : params) {
            if (Util.has(param.getRequestBody()))
                dataObjects.put(param.getParamName(), param.getParamValue());
            else {
                if (!sb.toString().isEmpty())
                    sb.append("&");
                sb.append(param.getParamName() + "=" + URLEncoder.encode(param.getParamValue(), "UTF-8"));
            }
        }

        HttpEntity<?> httpEntity = new HttpEntity<>(dataObjects, headers);
        HttpMethod method = HttpMethod.resolve(httpParams.getMethod());
        method = method == null ? HttpMethod.GET : method;
        if (!sb.toString().isEmpty())
            sb = new StringBuilder("?" + sb.toString());
        String url = httpParams.getUrl() + sb.toString();

        if (os != null) {
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
            restTemplate.getMessageConverters()
                    .add(new ByteArrayHttpMessageConverter());
            ResponseEntity<byte[]> httpResponse = restTemplate.exchange(url,
                    method, httpEntity, byte[].class);
            IOUtils.write(httpResponse.getBody(),
                    os);
        } else {
            ResponseEntity<String> httpResponse = restTemplate.exchange(url,
                    method, httpEntity, String.class);
            System.out.println(httpResponse.getBody());
        }
    }

    private static List<HttpParam> combineOptions(List<HttpParam> httpParams, Map<String, String> additionalOptions) {
        Map<String, String> opsPap = new HashMap<>();
        opsPap.putAll(additionalOptions);
        List<HttpParam> params = new ArrayList<>();
        for (HttpParam param : httpParams) {
            HttpParam p = new HttpParam(param.getParamName(), param.getParamValue(), param.getRequestBody(), param.getRequestPart());
            params.add(p);
            String value = opsPap.get(p.getParamName());
            if (value != null) {
                p.setParamValue(value);
                opsPap.remove(p.getParamName());
            }
        }
        for (String key : opsPap.keySet()) {
            HttpParam p = new HttpParam(key, opsPap.get(key));
            params.add(p);
        }
        return params;
    }
}
