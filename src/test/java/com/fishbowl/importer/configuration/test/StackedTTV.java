package com.fishbowl.importer.configuration.test;

/**
 * Created by dito on 5/13/16.
 */
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

public class StackedTTV extends Application {

    public static void main(String[] args) {launch(args);}

    @Override
    public void start(Stage stage) {
        final ObservableList<XYChart.Series<String, Number>> chartdata = FXCollections.observableArrayList();
        final CategoryAxis xAxis = new CategoryAxis(
                FXCollections.observableArrayList("Wait","Agreed","Work"));
        for (int i = 0; i < 4; i++) {
            chartdata.add(new XYChart.Series<>("Test"+i, FXCollections.observableArrayList(
                    new XYChart.Data(xAxis.getCategories().get(0), 1000),
                    new XYChart.Data(xAxis.getCategories().get(1), 1000),
                    new XYChart.Data(xAxis.getCategories().get(2), 1000)
            )));
        }
        final StackedBarChart sbc = new StackedBarChart(xAxis, new NumberAxis(), chartdata);

        final TreeTableView<XYChart.Data<String, Number>> ttv = new TreeTableView<>(
                new TreeItem<XYChart.Data<String,Number>>(new XYChart.Data<>()));
        ttv.setShowRoot(false);

        for (XYChart.Series<String, Number> serie: chartdata){
            TreeItem<XYChart.Data<String,Number>> ti = new TreeItem<>(new XYChart.Data<>(serie.getName(), null));
            ttv.getRoot().getChildren().add(ti);
            for (XYChart.Data<String,Number> data : serie.getData()){
                ti.getChildren().add(new TreeItem(data));
            }
        }

        TreeTableColumn<XYChart.Data<String,Number>,String> clientCol = new TreeTableColumn<>("client");
        clientCol.setCellValueFactory((param) -> {
            return param.getValue().isLeaf()
                    ? new SimpleStringProperty("")
                    : param.getValue().getValue().XValueProperty();
        });
        clientCol.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());
        clientCol.setOnEditCommit((evt) ->{
            if (evt.getRowValue().isLeaf()) return;
            for (XYChart.Series serie: chartdata){
                if (serie.getName().equals(evt.getOldValue()))
                    serie.setName(evt.getNewValue());
            }
            evt.getRowValue().getValue().setXValue(evt.getNewValue());
        });

        TreeTableColumn<XYChart.Data<String,Number>,String> titleCol = new TreeTableColumn<>("title");
        titleCol.setCellValueFactory((param) -> {
            return param.getValue().isLeaf()
                    ? param.getValue().getValue().XValueProperty()
                    : new SimpleStringProperty("");
        });
        titleCol.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());
        titleCol.setOnEditCommit((evt) -> {
            final String ov = evt.getOldValue();
            final String nv = evt.getNewValue();
            //change the name for all series
            for (XYChart.Series<String, Number> serie : chartdata)
                for (XYChart.Data<String, Number> data : serie.getData())
                    if(ov.equals(data.getXValue()))  data.setXValue(nv);
            xAxis.getCategories().set(xAxis.getCategories().indexOf(ov),nv);
            //chart is confused as to which categories to listen to
            //System.out.println(sbc.getXAxis().getTickMarks());
        });

        TreeTableColumn<XYChart.Data<String,Number>,Number> valueCol = new TreeTableColumn<>("value");
        valueCol.setCellValueFactory((param) -> {
            return param.getValue().getValue().YValueProperty();
        });
        valueCol.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn(new NumberStringConverter()));
        valueCol.setOnEditCommit((evt) ->{
            evt.getRowValue().getValue().setYValue(evt.getNewValue());
        });

        ttv.getColumns().addAll(clientCol,titleCol,valueCol);
        ttv.setColumnResizePolicy(TreeTableView.CONSTRAINED_RESIZE_POLICY);
        ttv.getSelectionModel().setCellSelectionEnabled(true);
        ttv.setEditable(true);

        final TextField txt = new TextField();
        txt.setPromptText("new title");
        txt.setOnAction((evt)->{
            //add to category axis
            //todo - check for dup
            xAxis.getCategories().add(txt.getText());
            //add new title to each series with 1000 and to table
            for (XYChart.Series<String, Number> serie : chartdata) {
                XYChart.Data<String, Number> newdata = new XYChart.Data<>(txt.getText(), 1000);
                serie.getData().add(newdata);
                for(TreeItem<XYChart.Data<String,Number>> ti:  ttv.getRoot().getChildren()){
                    if(ti.getValue().XValueProperty().get().equals(serie.getName())){
                        ti.getChildren().add(new TreeItem<>(newdata));
                    }
                }
            }
        });
        final VBox sceneRoot = new VBox(ttv,sbc,txt);
        final Scene scene = new Scene(sceneRoot);
        stage.setScene(scene);
        stage.show();
    }
}