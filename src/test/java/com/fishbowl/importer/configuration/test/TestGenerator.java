package com.fishbowl.importer.configuration.test;

import com.fishbowl.importer.configuration.Customers;
import com.fishbowl.importer.ui.UI;
import com.fishbowl.importer.utils.Creator;

/**
 * Created by dimitri.gamkrelidze on 4/20/2016.
 */
public class TestGenerator {
    public static void main(String[] args) throws Exception {
        Customers customers = Creator.createtCustomers();
        UI.startWithCustomerService(new CustomerServiceTest(customers));
    }
}
