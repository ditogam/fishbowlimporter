package com.fishbowl.importer.configuration.test;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by dito on 4/16/16.
 */
public class TestPDFItext {

    public static String getPageContent(String pdfPath) throws IOException {
        PdfReader reader = new PdfReader(pdfPath);
        StringWriter output = new StringWriter();
        try {
            for (int i = 0; i < reader.getNumberOfPages(); i++) {
                output.append(PdfTextExtractor.getTextFromPage(reader, i + 1));
            }
        } catch (OutOfMemoryError e) {

            e.printStackTrace();
        }
        return output.toString();
    }

    public static void main(String[] args) throws Exception {

        String str = getPageContent("/Users/dito/other/fishbowlimporter/src/test/resources/pdf.pdf");
        try (CSVParser parser = new CSVParser(new StringReader(str), CSVFormat.DEFAULT.withDelimiter(' '))) {
            for (CSVRecord strings : parser.getRecords()) {
                System.out.println(strings);
            }
        }


    }


}
