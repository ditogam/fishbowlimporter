package com.fishbowl.importer.configuration.test;

import com.fishbowl.importer.configuration.Customer;
import com.fishbowl.importer.configuration.Customers;
import com.fishbowl.importer.configuration.Pair;
import com.fishbowl.importer.service.CustomerService;
import com.fishbowl.importer.utils.Creator;

import java.util.List;

/**
 * Created by dimitri.gamkrelidze on 4/20/2016.
 */
public class CustomerServiceTest implements CustomerService {

    private List<Customer> list;
    private long id = 1;

    public CustomerServiceTest(Customers customers) {
        setCustomers(customers);
    }

    public void setCustomers(Customers customers) {
        list = customers.getCustomers();
        for (Customer customer : list) {
            customer.setId(id++);
        }
    }

    @Override
    public void save(Customer t) throws Exception {
        if (t.getId() == null) {
            t.setId(id++);
            list.add(t);
        } else {
            int index = getIndex(t.getId());
            if (index >= 0)
                list.set(index, t);
            else {
                t.setId(id++);
                list.add(t);
            }
        }
    }


    @Override
    public Customer getCustomer(Long id) throws Exception {
        int index = getIndex(id);
        if (index < 0)
            return null;
        Customer customer = list.get(index);
        return customer;
    }

    public int getIndex(Long id) {
        int index = -1;
        if (id == null)
            return -1;
        for (Customer customer : list) {
            index++;
            if (customer.getId() != null && customer.getId().longValue() == id.longValue())
                return index;
        }
        return -1;
    }

    @Override
    public List<Customer> getAll() throws Exception {
        return list;
    }

    @Override
    public void recreateDB() throws Exception {
        setCustomers(Creator.createtCustomers());
    }

    @Override
    public List<String> unique_items(Long customer_id, List<String> ids) {
        return null;
    }

    @Override
    public List<Pair<Long, String>> getAllFishblowCustomers() throws Exception {
        return null;
    }
}
