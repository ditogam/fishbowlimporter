package com.fishbowl.encryption;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.net.URL;

public class Encryptor extends Application {

    @FXML
    private PasswordField pfPassword;

    @FXML
    private TextField tfGenerated;

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    @FXML
    void generateAction(ActionEvent event) {
        generate();
    }

    private void generate() {
        try {
            String pwd=pfPassword.getText();
            tfGenerated.setText("ENSC(" + new AdvancedEncryptionStandard().encrypt(pwd) + ")");
            final Clipboard clipboard = Clipboard.getSystemClipboard();
            final ClipboardContent content = new ClipboardContent();
            content.clear();
            content.putString(tfGenerated.getText());
            clipboard.setContent(content);
        } catch (Exception e) {
            tfGenerated.setText(e.getMessage());
        }
    }

    @FXML
    void keyPressed(KeyEvent ke) {
        if (ke.getCode().equals(KeyCode.ENTER)) {
            generate();
        }

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = ClassLoader.getSystemClassLoader().getResource("password_generator.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        Parent root = loader.load();
        Scene mainScene = new Scene(root);
        primaryStage.setTitle("Password generator");
        primaryStage.setScene(mainScene);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }
}
